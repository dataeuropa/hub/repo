@file:JvmName("Indexing")

package io.piveau.hub.indexing

import io.piveau.dcatap.*
import io.piveau.json.*
import io.piveau.rdf.*
import io.piveau.vocabularies.vocabulary.*
import io.vertx.core.Future
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.rdf.model.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.vocabulary.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import io.piveau.profile.indexingResource
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.runBlocking


val log: Logger = LoggerFactory.getLogger("io.piveau.hub.Indexing")
lateinit var catalogInstr: JsonObject
lateinit var datasetInstr: JsonObject

fun init(catalogInstructions: JsonObject, datasetInstructions: JsonObject) {
    catalogInstr = catalogInstructions
    datasetInstr = datasetInstructions
}

fun indexingCatalogue(catalogue: Resource): Future<JsonObject> {
    return indexingResource(
        catalogue,
        null,
        catalogInstr,
        "en",
        "DCATAP",
        false,
        null,
        true
    )
}

fun indexingDataset(
    dataset: Resource,
    catalogRecord: Resource,
    catalogueId: String? = null,
    defaultLang: String? = "en",
): JsonObject {

    var result = runBlocking {
        indexingResource(
            dataset,
            catalogRecord,
            datasetInstr,
            defaultLang ?: "en",
            "DCATAP",
            false,
            catalogueId,
            false
        ).coAwait()
    }
    return result
}


fun indexingMetrics(metrics: Model): JsonObject = JsonObject().apply {
    metrics.listResourcesWithProperty(DQV.hasQualityMeasurement).forEach {
        if (it.isURIResource) {
            if (DCATAPUriSchema.isDatasetUriRef(it.uri)) {
                putIfNotBlank("id", DCATAPUriSchema.parseUriRef(it.uri).id)
            }

            it.listProperties().forEachRemaining { (_, predicate, obj) ->
                when (predicate) {
                    DQV.hasQualityAnnotation -> withJsonArray("quality_ann").addAll(indexingAnnotations(obj))
                    DQV.hasQualityMeasurement -> withJsonObject("quality_meas").apply {
                        when (val name = measurementName(obj)) {
                            "scoring" -> putIfNotNull(name, indexingScore(obj))
                            "" -> {}
                            else -> {
                                withJsonArray(name).addIfNotEmpty(indexingMeasurements(obj))
                            }
                        }
                    }
                }
            }
        }
    }
}

fun measurementName(node: RDFNode): String = when (node) {
    is Resource -> node.asResource().getProperty(DQV.isMeasurementOf)?.resource?.localName ?: ""
    else -> ""
}

fun indexingScore(node: RDFNode): Int? = when (node) {
    is Resource -> node.asResource().getProperty(DQV.value)?.`object`?.asLiteral()?.int
    else -> null
}

fun indexingMeasurements(node: RDFNode): JsonObject = when (node) {
    is Resource -> JsonObject().apply {
        val resource = node.asResource()
        putIfNotBlank("computedOn", resource.getProperty(DQV.computedOn)?.resource?.uri)
        putIfNotBlank("date", resource.getProperty(PROV.generatedAtTime)?.string)
        when (resource.getProperty(DQV.value).`object`.asLiteral().datatype) {
            XSDDatatype.XSDboolean -> putIfNotNull("value", resource.getProperty(DQV.value)?.boolean)
            XSDDatatype.XSDinteger -> putIfNotNull("value", resource.getProperty(DQV.value)?.int)
            else -> JsonObject()
        }
    }

    else -> JsonObject()
}

fun indexingAnnotations(node: RDFNode): JsonArray = when (node) {
    is Resource -> JsonArray().apply {
        node.getProperty(OA.hasBody).resource.listProperties(SHACL.result)
            .forEach {
                add(JsonObject().apply {
                    putIfNotBlank("resultMessage", it.subject.asResource().getProperty(SHACL.resultMessage)?.string)
                    putIfNotBlank(
                        "resultSeverity",
                        it.subject.asResource().getProperty(SHACL.resultSeverity)?.resource?.localName
                    )
                    putIfNotBlank(
                        "sourceConstraintComponent",
                        it.subject.getProperty(SHACL.sourceConstraintComponent)?.resource?.localName
                    )
                })
            }
    }

    else -> JsonArray()
}