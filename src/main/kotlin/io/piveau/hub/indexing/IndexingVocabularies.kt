/*
 * Copyright (c) 2023. European Commission
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package io.piveau.hub.indexing

import io.piveau.rdf.*
import io.piveau.json.putIfNotEmpty
import io.piveau.json.putIfNotNull
import io.piveau.vocabularies.vocabulary.PVLA
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.*
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DC_11
import org.apache.jena.vocabulary.RDF
import org.apache.jena.vocabulary.SKOS

fun indexConceptScheme(id: String, uriRef: String, model: Model): JsonObject {
    val index = JsonObject()
        .put("resource", uriRef)
        .put("id", id)
    val vocab = JsonArray()
    index.put("vocab", vocab)

    model.listSubjectsWithProperty(RDF.type, SKOS.Concept).forEachRemaining { concept ->
        val conceptIndex = indexConcept(concept)
        indexAddendums(concept, conceptIndex)
        if (conceptIndex.containsKey("pref_label")) {
            if (!conceptIndex.containsKey("in_scheme")) {
                conceptIndex.put("in_scheme", uriRef)
            }

            vocab.add(conceptIndex)
        }
    }

    return index
}

fun indexConcept(concept: Resource): JsonObject {
    val index = JsonObject()
        .put("resource", concept.uri)

    concept.getProperty(DC_11.identifier)?.let {
        index.put("id", it.literal.lexicalForm)
    } ?: index.put("id", concept.uri.substringAfterLast("/", concept.uri))

    concept.getPropertyResourceValue(SKOS.inScheme)?.let {
        index.put("in_scheme", it.uri)
    }

    concept.getProperty(FOAF.homepage)?.let {
        index.put("extensions", JsonObject().put("foaf_homepage", it.resource.uri))
    }

    // pref_label
    val prefLabels = JsonObject()
    concept.listProperties(SKOS.prefLabel).toList()
        .map { it.literal }
        .filter { it.language.length == 2 }
        .forEach { prefLabels.put(it.language, it.string) }
    index.putIfNotEmpty("pref_label", prefLabels)

    // alt_label
    val altLabels = JsonObject()
    concept.listProperties(SKOS.altLabel).toList()
        .map { it.literal }
        .filter { it.language.length == 2 }
        .forEach { altLabels.put(it.language, it.string) }
    index.putIfNotEmpty("alt_label", altLabels)

    return index
}

fun indexAddendums(concept: Resource, index: JsonObject): JsonObject {
    if (concept.hasProperty(PVLA.addendum)) {
        val addendum = JsonObject()

        concept.getPropertyResourceValue(PVLA.addendum)?.let {
            it.listProperties().forEach { (_, predicate, obj) ->
                when (predicate) {
                    PVLA.permissions -> addendum.putIfNotEmpty("permissions", obj.visitWith(PermissionsVisitor) as JsonObject)
                    PVLA.requirements -> addendum.putIfNotEmpty("requirements", obj.visitWith(RequirementsVisitor) as JsonObject)
                    PVLA.prohibitions -> addendum.putIfNotEmpty("prohibitions", obj.visitWith(ProhibitionsVisitor) as JsonObject)
                    PVLA.databaseRightsCovered -> addendum.putIfNotNull("database_rights_covered", obj.assumeBoolean())
                }
            }
        }
        if (!addendum.isEmpty) {
            index.put("addendum", addendum)
        }
    }
    return index
}

internal object PermissionsVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, anonId: AnonId): Any = visitPermissions(resource)

    override fun visitURI(resource: Resource, uriRef: String): Any = visitPermissions(resource)

    override fun visitLiteral(value: Literal): Any = JsonObject()

    private fun visitPermissions(resource: Resource): JsonObject {
        val permissions = JsonObject()
        resource.listProperties().forEach { (_, predicate, obj) ->
            when (predicate) {
                PVLA.reproduction -> permissions.putIfNotNull("reproduction", obj.assumeBoolean())
                PVLA.distribution -> permissions.putIfNotNull("distribution", obj.assumeBoolean())
                PVLA.derivativeWorks -> permissions.putIfNotNull("derivative_works", obj.assumeBoolean())
                PVLA.sublicensing -> permissions.putIfNotNull("sublicensing", obj.assumeBoolean())
                PVLA.patentGrant -> permissions.putIfNotNull("patent_grant", obj.assumeBoolean())
            }
        }
        return permissions
    }

}

internal object RequirementsVisitor : RDFVisitor {
    override fun visitBlank(ressource: Resource, anonId: AnonId): Any = visitRequirements(ressource)

    override fun visitURI(resource: Resource, uriRef: String): Any = visitRequirements(resource)

    override fun visitLiteral(value: Literal): Any = JsonObject()

    private fun visitRequirements(resource: Resource): JsonObject {
        val requirements = JsonObject()
        resource.listProperties().forEach { (_, predicate, obj) ->
            when (predicate) {
                PVLA.notice -> requirements.putIfNotNull("notice", obj.assumeBoolean())
                PVLA.attribution -> requirements.putIfNotNull("attribution", obj.assumeBoolean())
                PVLA.shareAlike -> requirements.putIfNotNull("share_alike", obj.assumeBoolean())
                PVLA.copyleft -> requirements.putIfNotNull("copyleft", obj.assumeBoolean())
                PVLA.lesserCopyleft -> requirements.putIfNotNull("lesser_copyleft", obj.assumeBoolean())
                PVLA.stateChanges -> requirements.putIfNotNull("state_changes", obj.assumeBoolean())
            }
        }
        return requirements
    }

}

internal object ProhibitionsVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, anonId: AnonId): Any = visitProhibitions(resource)

    override fun visitURI(resource: Resource, uriRef: String): Any = visitProhibitions(resource)

    override fun visitLiteral(value: Literal): Any = JsonObject()

    private fun visitProhibitions(resource: Resource): JsonObject {
        val prohibitions = JsonObject()
        resource.listProperties().forEach { (_, predicate, obj) ->
            when (predicate) {
                PVLA.commercial -> prohibitions.putIfNotNull("commercial", obj.assumeBoolean())
                PVLA.useTrademark -> prohibitions.putIfNotNull("use_trademark", obj.assumeBoolean())
            }
        }
        return prohibitions
    }

}

fun RDFNode.assumeBoolean(): Boolean? = if (isLiteral) asLiteral().boolean else null
