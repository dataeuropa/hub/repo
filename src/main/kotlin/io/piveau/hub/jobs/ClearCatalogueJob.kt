package io.piveau.hub.jobs

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.LogWrapper
import io.piveau.hub.services.datasets.DatasetsService
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.asFlow

class ClearCatalogueJob(
    vertx: Vertx,
    val tripleStore: TripleStore,
    log: LogWrapper,
    val catalogueId: String
) : Job(log) {

    private val datasetService: DatasetsService =
        DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS)

    override suspend fun execute(): String {
        log.info("Clearing catalogue $catalogueId")

        val catalogueManager = tripleStore.catalogueManager

        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)

        val datasets = catalogueManager.allDatasets(catalogueUriRef.id).coAwait().toSet()
        log.info("Found ${datasets.size} dataset entries in catalogue ${catalogueUriRef.id}")
        datasets
            .asFlow()
            .collect { uriRef ->
                try {
                    datasetService.deleteDataset(uriRef.id).coAwait()
                    log.info("Dataset ${uriRef.id} deleted")
                } catch (e: Exception) {
                    log.error("Dataset ${uriRef.id} deletion failure: ${e.message}")
                }
            }

        return "1"
    }

    override suspend fun status(): JsonObject = JsonObject()

}