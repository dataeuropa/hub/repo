package io.piveau.hub.jobs

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.Defaults
import io.piveau.hub.LogWrapper
import io.piveau.hub.indexing.indexingCatalogue
import io.piveau.hub.indexing.indexingDataset
import io.piveau.hub.services.index.IndexService
import io.piveau.vocabularies.Concept
import io.piveau.vocabularies.Languages
import io.piveau.vocabularies.vocabulary.EDP
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.transform
import org.apache.jena.vocabulary.DCTerms
import java.util.concurrent.atomic.AtomicInteger

class IndexJob(
    vertx: Vertx,
    val tripleStore: TripleStore,
    log: LogWrapper,
    val catalogues: List<String> = emptyList(),
    val deep: Boolean = false,
    val chunkSize: Int = 100,
    val verbose: Boolean = false
) : Job(log) {

    private val indexService: IndexService =
        IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS, IndexService.DEFAULT_TIMEOUT)

    override suspend fun execute(): String {
        catalogues.forEach {
            indexCatalogue(it, deep)
        }
        return "1"
    }

    override suspend fun status(): JsonObject = JsonObject()

    private suspend fun indexCatalogue(catalogueId: String, deep: Boolean) {
        log.info("Indexing catalogue $catalogueId")

        val catalogueManager = tripleStore.catalogueManager
        val datasetManager = tripleStore.datasetManager

        val catalogueModel = catalogueManager.getStripped(catalogueId).coAwait()
        if (catalogueModel.isEmpty) {
            log.warn("Catalogue $catalogueId not found")
            return
        }

        val subCatalogues = catalogueManager.subCatalogues(catalogueId).coAwait().map { it.id }
        if (deep && subCatalogues.isNotEmpty()) {
            subCatalogues.forEach { indexCatalogue(it, true) }
        }

        val catalogueResource = catalogueModel.getResource(DCATAPUriSchema.createForCatalogue(catalogueId).catalogueUriRef)

        if (catalogueResource.hasProperty(EDP.visibility, EDP.hidden)) {
            log.warn("Catalogue $catalogueId is hidden, and hence skipped for indexing")
        } else {
            val catalogueIndex = indexingCatalogue(catalogueResource).coAwait()
            indexService.addCatalog(catalogueIndex).coAwait()

            log.info("Catalogue metadata for $catalogueId indexed successfully")

            val defaultLang = try {
                val lang = catalogueModel.listObjectsOfProperty(DCTerms.language).next().asResource()
                Languages.iso6391Code(
                    Languages.getConcept(lang) ?: Languages.getConcept(Defaults.DEFAULT_LANG) as Concept
                ) ?: "en"
            } catch (e: Exception) {
                log.error("Error while getting default language for catalogue $catalogueId", e)
                "en"
            }

            val datasets = catalogueManager.allDatasets(catalogueId).coAwait().toSet()
            log.info("Indexing ${datasets.size} datasets")

            val counter = AtomicInteger(0)

            datasets.chunked(chunkSize).forEach { chunk ->
                val datasetsBulkRequest = JsonArray()

                chunk.asFlow()
                    .transform { uriSchema ->
                        try {
                            val model = datasetManager.getGraph(uriSchema.uriRef).coAwait()
                            emit(uriSchema to model)
                        } catch (e: Exception) {
                            log.error("Get dataset failure: ${uriSchema.id} - ${e.message}")
                        }
                    }
                    .collect { (uriSchema, model) ->
                        try {
                            val value = indexingDataset(
                                    model.getResource(uriSchema.datasetUriRef),
                                    model.getResource(uriSchema.recordUriRef),
                                    catalogueId,
                                    defaultLang
                                )
                            datasetsBulkRequest.add(value)
                        } catch (e: Exception) {
                            log.error("Indexing dataset failure: ${uriSchema.id} - ${e.stackTraceToString()}")
                        }
                    }

                try {
                    val bulkResult = indexService.putDatasetBulk(datasetsBulkRequest).coAwait()
                    bulkResult
                        .map { it as JsonObject }
                        .filter {
                            val status = it.getInteger("status")
                            status != 200 && status != 201
                        }
                        .forEach { dataset ->
                            log.error("Indexing dataset failure: ${dataset.getString("id")} - ${dataset.getString("message")}")
                        }
                } catch (e: Exception) {
                    log.error("Indexing bulk request failure: ${e.message}")
                }

                log.info("Processed ${counter.addAndGet(chunk.size)}")
            }
            log.info("Datasets of $catalogueId indexed successfully")

            val indexIds = catalogueIndexIds(catalogueId)
            val datasetIds = catalogueStoreIds(catalogueId)

            val invalidIds = indexIds - datasetIds
            invalidIds.forEach {
                try {
                    indexService.deleteDataset(it).coAwait()
                } catch (e: Exception) {
                    log.warn("Delete $it from index: ${e.message}")
                }
            }
        }

    }

    private suspend fun catalogueIndexIds(catalogueId: String): Set<String> {
        return indexService.listDatasets(catalogueId, "dataset_write")
            .map { datasets -> datasets.map { it as String }.toSet() }
            .coAwait()
    }

    private suspend fun catalogueStoreIds(catalogueId: String): Set<String> {
        val datasets = tripleStore.catalogueManager.allDatasets(catalogueId).coAwait()
        return datasets.map { it.id }.toSet()
    }

}
