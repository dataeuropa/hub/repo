package io.piveau.hub.jobs

import io.piveau.dcatap.TripleStore
import io.piveau.hub.Constants
import io.piveau.hub.LogWrapper
import io.piveau.hub.services.catalogues.CataloguesService
import io.piveau.hub.services.datasets.DatasetsService
import io.piveau.hub.services.vocabularies.VocabulariesService
import io.piveau.hub.vocabularies.fetchFromRemote
import io.piveau.hub.vocabularies.mergeExtensions
import io.piveau.hub.vocabularies.updateVocabularyDataset
import io.piveau.json.asJsonObject
import io.piveau.rdf.*
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import org.apache.jena.riot.Lang
import kotlin.io.path.Path
import kotlin.reflect.cast

class InstallVocabulariesJob(
    val vertx: Vertx,
    val tripleStore: TripleStore,
    log: LogWrapper,
    val override: Boolean = false,
    val remote: Boolean = false,
    val tags: List<String> = listOf("core")
) : Job(log) {

    override suspend fun execute(): String {
        log.info("Installing vocabularies")

        val client = WebClient.create(vertx)

        val vocabulariesService = VocabulariesService.createProxy(vertx, VocabulariesService.SERVICE_ADDRESS)
        val datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS)
        val cataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS)

        val vocabularies = vertx.fileSystem().readFile("vocabularies.json").coAwait().toJsonArray()

        vocabularies
            .map { it as JsonObject }
            .filter {
                val vocabularyTags = it.getJsonArray("tags", JsonArray()).map(String::class::cast).toSet()
                tags.any { tag -> vocabularyTags.contains(tag) }
            }
            .forEach info@ { info ->
                val id = info.getString("id")
                val uriRef = info.getString("uriRef")

                log.info("Installing $id ($uriRef)...")

                if (override || !tripleStore.datasetManager.existGraph(uriRef).coAwait()) {
                    val filename = when {
                        remote && info.containsKey("remote") -> fetchFromRemote(
                            vertx,
                            client,
                            info.getJsonObject("remote", JsonObject())
                        ).coAwait()

                        info.containsKey("file") -> info.getString("file")
                        else -> {
                            log.error("Vocabulary $id is not properly configured")
                            return@info
                        }
                    }

                    val status = Path(filename).loadToModel(vertx)
                        .compose { model -> mergeExtensions(model, info, vertx) }
                        .compose { model -> model.saveToTmpFile(Lang.TURTLE, vertx) }
                        .compose { tmpFilename ->
                            vocabulariesService.putVocabularyFile(
                                id,
                                uriRef,
                                RDFMimeTypes.TURTLE,
                                tmpFilename
                            )
                        }
                        .compose { status ->
                            log.info("Vocabulary $id $status")
                            updateVocabularyDataset(datasetsService, cataloguesService, uriRef, id)
                        }.coAwait()

                    log.info("Vocabulary $id related dataset ${status.getString("status")}")
                } else {
                    log.warn("Vocabulary $id already stored")
                }
            }
        return "1"
    }

    override suspend fun status(): JsonObject = JsonObject()

    suspend fun list() {
        val vocabularies = vertx.fileSystem().readFile("vocabularies.json").coAwait().toJsonArray()
        log.info(vocabularies.encodePrettily())
    }

}