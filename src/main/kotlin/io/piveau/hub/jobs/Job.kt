package io.piveau.hub.jobs

import io.piveau.hub.LogWrapper
import io.piveau.hub.SLF4JLog
import io.vertx.core.json.JsonObject

abstract class Job(val log: LogWrapper) {
    abstract suspend fun execute(): String
    abstract suspend fun status(): JsonObject
}

class VoidJob : Job(SLF4JLog(VoidJob::javaClass.name)) {
    override suspend fun execute() = "void"
    override suspend fun status() = JsonObject()
}
