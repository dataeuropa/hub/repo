package io.piveau.hub.jobs

import io.piveau.dcatap.TripleStore
import io.piveau.hub.Constants
import io.piveau.hub.SLF4JLog
import io.piveau.json.asJsonObject
import io.piveau.utils.FriendlyWords
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineEventBusSupport
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.slf4j.LoggerFactory
import kotlin.reflect.cast

class JobVerticle : CoroutineVerticle(), CoroutineEventBusSupport {

    override suspend fun start() {
        val config = vertx.orCreateContext.config()
        val tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        vertx.eventBus().coConsumer<JsonObject>(ADDRESS) { message ->
            val body = message.body()
            val action = body.getString("action", "")
            val params = body.getJsonObject("params")

            val friendlyName = FriendlyWords().generateOutOfTwo()

            val job = when (action) {
                "buildIndex" -> IndexJob(
                    vertx,
                    tripleStore,
                    SLF4JLog("buildIndex~$friendlyName"),
                    params.getJsonArray("catalogues").map(String::class::cast),
                    params.getBoolean("deep", false)
                )

                "installVocabularies" -> InstallVocabulariesJob(
                    vertx,
                    tripleStore,
                    SLF4JLog("installVocabularies~$friendlyName")
                )
//                "repair" -> RepairJob(
//                    ,
//                    params.getJsonArray("catalogues").map(String::class::cast),
//                    SLF4JLog("repair~$friendlyName")
//                )
                else -> VoidJob()
            }

            job.execute()
        }
    }

    companion object {
        const val ADDRESS = "io.piveau.hub.job.service"
    }

}