@file:JvmName("Vocabularies")
/*
 * Copyright (c) 2023. European Commission
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package io.piveau.hub.vocabularies

import io.piveau.dcatap.DatasetManager
import io.piveau.hub.services.catalogues.CataloguesService
import io.piveau.hub.services.datasets.DatasetsService
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.findDatasetAsDCATAPUriRef
import io.piveau.rdf.loadToModel
import io.piveau.rdf.presentAs
import io.piveau.rdf.toModel
import io.piveau.utils.dcatap.dsl.catalogue
import io.piveau.utils.dcatap.dsl.dataset
import io.piveau.vocabularies.vocabulary.EDP
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import java.time.Instant
import java.util.concurrent.atomic.AtomicReference
import kotlin.io.path.Path

private const val VOCABULARIES_CATALOGUE_ID = "vocabularies"

val vocabulariesCatalogue = catalogue(VOCABULARIES_CATALOGUE_ID) {
    title { "Vocabularies" lang "en" }
    description { "DCAT-AP related Vocabularies" lang "en" }
    language { +"ENG" }
    publisher {
        name { "Data Europa EU" lang "en" }
        addProperty(DCTerms.type, model.createResource("http://purl.org/adms/publishertype/NationalAuthority"))
    }
    addProperty(DCTerms.type, "dcat-ap")
    addProperty(EDP.visibility, EDP.hidden)
}

fun fetchFromRemote(vertx: Vertx, client: WebClient, remote: JsonObject): Future<String> {
    val file = AtomicReference<String>()
    return vertx.fileSystem().createTempFile("piveau", ".tmp")
        .compose { filename ->
            file.set(filename)
            vertx.fileSystem().open(filename, OpenOptions().setWrite(true))
        }.compose { stream ->
            val request = client.getAbs(remote.getString("address"))
                .`as`(BodyCodec.pipe(stream, true))

            if (remote.containsKey("mimetype")) {
                request.putHeader(HttpHeaders.ACCEPT.toString(), remote.getString("mimetype"))
            }

            request
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
        }
        .map(file.get())
}

fun loadFromLocal(vertx: Vertx, file: String): Future<String> = vertx.fileSystem().readFile(file).map { it.toString() }

fun updateVocabularyDataset(
    datasetsService: DatasetsService,
    cataloguesService: CataloguesService,
    vocabularyUriRef: String,
    vocabularyId: String
): Future<JsonObject> {
    return cataloguesService.getCatalogue(VOCABULARIES_CATALOGUE_ID, RDFMimeTypes.NTRIPLES)
        .recover { _ ->
            cataloguesService.putCatalogue(
                VOCABULARIES_CATALOGUE_ID,
                vocabulariesCatalogue.model.presentAs(Lang.NTRIPLES),
                RDFMimeTypes.NTRIPLES
            )
        }
        .compose { _ ->
            datasetsService.getDatasetOrigin(vocabularyId, VOCABULARIES_CATALOGUE_ID, RDFMimeTypes.NTRIPLES)
                .map {
                    val model = it.encodeToByteArray().toModel(Lang.NTRIPLES)
                    val resource = model.getResource(model.findDatasetAsDCATAPUriRef()?.uriRef)

                    resource?.removeAll(DCTerms.modified)
                    resource?.addProperty(DCTerms.modified, Instant.now().toString(), XSDDatatype.XSDdateTime)

                    model
                }
        }
        .recover { _ ->
            val model = dataset(vocabularyId) {
                title { vocabularyId lang "en" }
                description { "DCAT-AP related vocabulary" lang "en" }
                distribution {
                    addProperty(DCAT.accessURL, ResourceFactory.createResource(vocabularyUriRef))
                }
                addProperty(DCTerms.issued, Instant.now().toString(), XSDDatatype.XSDdateTime)
                addProperty(DCTerms.modified, Instant.now().toString(), XSDDatatype.XSDdateTime)
            }.model
            Future.succeededFuture(model)
        }
        .compose { model ->
            datasetsService.putDatasetOrigin(
                vocabularyId,
                model.presentAs(Lang.NTRIPLES),
                RDFMimeTypes.NTRIPLES,
                VOCABULARIES_CATALOGUE_ID,
                false)
        }
}

fun mergeExtensions(model: Model, info: JsonObject, vertx: Vertx): Future<Model> {
    return if (info.containsKey("extensions")) {
        val futures = info.getJsonArray("extensions", JsonArray())
            .map { extension ->
                Path(extension as String)
                    .loadToModel(vertx)
                    .onSuccess { model.add(it) }
            }
            .toList()
        Future.join(futures).map(model)
    } else {
        Future.succeededFuture(model)
    }
}

fun findVocabularyUriRef(vocabularyId: String, datasetManager: DatasetManager): Future<String> =
    datasetManager.get(vocabularyId, VOCABULARIES_CATALOGUE_ID)
        .map { model ->
            when {
                model.isEmpty -> throw Exception("Vocabulary $vocabularyId not found")
                else -> {
                    model.listObjectsOfProperty(DCAT.accessURL)
                        .toList()
                        .stream()
                        .filter(RDFNode::isURIResource)
                        .findFirst()
                        .orElseThrow().asResource().uri
                }
            }
        }
