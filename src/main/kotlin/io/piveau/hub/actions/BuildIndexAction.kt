package io.piveau.hub.actions

import io.piveau.dcatap.TripleStore
import io.piveau.hub.Constants
import io.piveau.hub.SLF4JLog
import io.piveau.hub.jobs.IndexJob
import io.piveau.json.asJsonObject
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.vertxFuture
import kotlin.reflect.cast

class BuildIndexAction : JSONRPCMethod("buildIndex") {
    private var running = false
    override fun execute(vertx: Vertx, parameters: JsonObject): Future<JsonObject> = vertxFuture(vertx) {
        if (running) {
            throw JSONRPCException("Already running")
        }
        running = true
        val logger = SLF4JLog(javaClass.name)
        val catalogues = parameters.getJsonArray("catalogues", JsonArray()).map(String::class::cast)
        val deep = parameters.getBoolean("deep", false)
        val chunkSize = parameters.getInteger("chunkSize", 100)
        val verbose = parameters.getBoolean("verbose", false)

        val config = vertx.orCreateContext.config()
        val tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        val job = IndexJob(vertx, tripleStore, logger, catalogues, deep, chunkSize, verbose)
        val id = job.execute()

        running = false
        JsonObject().put("reference", id)
    }
}
