package io.piveau.hub.actions

class JSONRPCException(message: String) : Exception(message) {
}
