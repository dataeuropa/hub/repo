package io.piveau.hub.handler;

import io.piveau.hub.actions.JSONRPCDispatcher;
import io.piveau.hub.util.ContentType;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.impl.MimeMapping;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.MIMEHeader;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;


public class ActionHandler {

    private final JSONRPCDispatcher actionDispatcher;

    public ActionHandler(Vertx vertx) {
        actionDispatcher = JSONRPCDispatcher.create(vertx);
    }

    public void handlePostAction(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        JsonObject jsonrpcRequest = parameters.body().getJsonObject();

        actionDispatcher.dispatch(jsonrpcRequest)
                .onSuccess(result -> {
                    if (result.isEmpty()) {
                        context.response()
                                .setStatusCode(202)
                                .end();
                    } else {
                        context.response()
                                .setStatusCode(200)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .end(result.encodePrettily());
                    }
                })
                .onFailure(context::fail);
    }

}
