package io.piveau.hub.handler;

import io.piveau.HubRepo;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.drafts.DatasetDraftsService;
import io.piveau.hub.util.ContentNegotiation;
import io.piveau.rdf.Piveau;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DatasetDraftHandler {

    private final DatasetsService datasetsService;
    private final DatasetDraftsService datasetDraftsService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetDraftHandler.class);

    public DatasetDraftHandler(Vertx vertx) {
        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        datasetDraftsService = DatasetDraftsService.createProxy(vertx, DatasetDraftsService.SERVICE_ADDRESS);
    }

    public void listDatasetDrafts(RoutingContext context) {
        try {
            boolean filterByProvider = !context.queryParam("filterByProvider").isEmpty() &&
                    !context.queryParam("filterByProvider").get(0).equals("false");

            List<String> authorizedResources = context.queryParam("authorizedResources");
            String provider = filterByProvider ? context.queryParam("provider").get(0) : "";

            datasetDraftsService.listDatasetDrafts(authorizedResources, provider)
                    .onSuccess(context::json)
                    .onFailure(cause -> HubRepo.failureResponse(context, cause));
        } catch (Exception e) {
            HubRepo.failureResponse(context, new ServiceException(500, "Internal Server Error"));
        }
    }

    public void createDatasetDraft(RoutingContext context) {
        String catalogueId = context.queryParam("catalogue").get(0);
        String contentType = context.parsedHeaders().contentType().value();

        String provider = context.queryParam("provider").get(0);

        datasetDraftsService.createDatasetDraft(catalogueId, context.body().asString(), contentType, provider)
                .onSuccess(result -> context.response().setStatusCode(201).end())
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    public void readDatasetDraft(RoutingContext context) {
        ContentNegotiation contentNegotiation = new ContentNegotiation(context);
        String datasetId = contentNegotiation.getId();
        String acceptType = contentNegotiation.getAcceptType();

        String catalogueId = context.queryParam("catalogue").get(0);

        datasetDraftsService.readDatasetDraft(datasetId, catalogueId, acceptType)
                .onSuccess(result -> context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE.toString(), acceptType)
                        .end(result))
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    public void createOrUpdateDatasetDraft(RoutingContext context) {
        String datasetId = context.pathParam("id");
        String catalogueId = context.queryParam("catalogue").get(0);

        String contentType = context.parsedHeaders().contentType().value();

        String provider = context.queryParam("provider").isEmpty() ? "" : context.queryParam("provider").get(0);

        datasetDraftsService.createOrUpdateDatasetDraft(datasetId, catalogueId, context.body().asString(), contentType, provider)
                .onSuccess(result -> {
                    switch (result) {
                        case "created" -> context.response().setStatusCode(201).end();
                        case "updated" -> context.response().setStatusCode(204).end();
                        default -> context.response().setStatusCode(400).end();
                    }
                })
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    public void deleteDatasetDraft(RoutingContext context) {
        String datasetId = context.pathParam("id");
        String catalogueId = context.queryParam("catalogue").get(0);

        HttpServerResponse response = context.response();
        datasetDraftsService.deleteDatasetDraft(datasetId, catalogueId)
                .onSuccess(v -> response.setStatusCode(204).end())
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    public void publishDatasetDraft(RoutingContext context) {
        String datasetId = context.pathParam("id");
        String catalogueId = context.queryParam("catalogue").get(0);
        String acceptType = "application/n-triples";

        datasetDraftsService.readDatasetDraft(datasetId, catalogueId, acceptType)
                .compose(content -> datasetsService.putDatasetOrigin(datasetId, removeCatalogRecord(content, acceptType),
                                acceptType, catalogueId, false))
                .compose(result -> datasetDraftsService.deleteDatasetDraft(datasetId, catalogueId))
                .onSuccess(v -> context.response().setStatusCode(204).end())
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    public void hideDataset(RoutingContext context) {
        String datasetId = context.pathParam("id");
        String catalogueId = context.queryParam("catalogue").get(0);
        String acceptType = "application/n-triples";

        String provider = context.queryParam("provider").isEmpty() ? "" : context.queryParam("provider").get(0);

        datasetsService.getDatasetOrigin(datasetId, catalogueId, acceptType)
                .compose(content ->
                        datasetDraftsService.createOrUpdateDatasetDraft(datasetId, catalogueId, content, acceptType, provider))
                .compose(result -> datasetsService.deleteDatasetOrigin(datasetId, catalogueId))
                .onSuccess(v -> context.response().setStatusCode(204).end())
                .onFailure(cause -> HubRepo.failureResponse(context, cause));
    }

    private String removeCatalogRecord(String payload, String acceptType) {

        Model model = Piveau.toModel(payload.getBytes(), acceptType);
        ResIterator resIterator = model.listSubjectsWithProperty(RDF.type, DCAT.CatalogRecord);
        if (resIterator.hasNext()) {
            Resource catalogRecord = resIterator.next();
            List<Statement> toBeRemoved = new ArrayList<>();
            calcToBeRemoved(catalogRecord, toBeRemoved, model);
            model.remove(toBeRemoved);
            return Piveau.presentAs(model, acceptType);
        }
        return payload;

    }

    private void calcToBeRemoved(Resource rdfNode, List<Statement> toBeRemoved, Model model) {
        try {
            StmtIterator stmtIterator = model.listStatements();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                if (statement.getSubject().equals(rdfNode)) {
                    toBeRemoved.add(statement);
                    if (statement.getObject().isAnon()) {
                        calcToBeRemoved(statement.getObject().asResource(), toBeRemoved, model);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("DatasetDraftHandler :: calcToBeRemoved  :: error :: {}", e.getMessage(), e);
            throw e;
        }
    }
}
