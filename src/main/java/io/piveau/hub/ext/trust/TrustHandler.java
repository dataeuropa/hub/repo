/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */


package io.piveau.hub.ext.trust;

import com.nimbusds.jose.*;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import io.piveau.hub.Constants;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.ext.trust.services.*;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.resources.*;
import io.piveau.hub.util.*;
import io.piveau.rdf.*;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileSystem;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.validation.*;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrustHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Vertx vertx;
    private DatasetsService datasetsService;
    private ResourcesService resourcesService;
    private JsonObject config;
    private WebClient client;

    private String baseUri;
    private Model mod = ModelFactory.createDefaultModel();

    public String did;

    public RSAKey rsaPublicKey;
    public StorageService storageService;
    private VCService vcService;
    private ComplianceService complianceService;
    private DIDService didService;
    private SetupService setupService;
    private TrustService trustService;

    public TrustHandler(
            Vertx vertx,
            JsonObject config
    ) {
        this.vertx = vertx;
        this.client = WebClient.create(vertx);
        this.config = config.getJsonObject(Constants.ENV_PIVEAU_TRUST_CONFIG);

        JsonObject schemaConfig = config.getJsonObject(Constants.ENV_PIVEAU_DCATAP_SCHEMA_CONFIG, new JsonObject());
        this.baseUri = schemaConfig.getString("baseUri", "https://piveau.io/") + schemaConfig.getString("resource", "set/resource/");

        resourcesService = ResourcesService.createProxy(vertx, ResourcesService.SERVICE_ADDRESS);
        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        if (this.config.containsKey("key")) {
            try {
                // Get RSA Private and Public Keys
                JWK jwk = JWK.parseFromPEMEncodedObjects(this.config.getString("key"));
                RSAKey rsaPrivateKey = jwk.toRSAKey();
                RSAKey rsaPublicKey = rsaPrivateKey.toPublicJWK();
                this.rsaPublicKey = rsaPublicKey;
                this.did = this.config.getString("did");
                String addressVCSignService = this.config.getString("addressSignService");

                // Setup Storage Service
                FileSystem fileSystem = vertx.fileSystem();
                this.storageService = new StorageService(fileSystem);

                // Setup DID Service
                String pathDid = "/trust/did";
                this.didService = new DIDService(
                    pathDid,
                    storageService
                );

                // Setup VC Service (Used for signing/verifying VC documents)
                this.vcService = new VCService(
                    rsaPrivateKey,
                    rsaPublicKey,
                    client,
                    addressVCSignService
                );

                // Setup compliance service for submitting/storing compliance VCs to Gaia-X
                this.complianceService = new ComplianceService(
                    client
                );

                // Setup Service to create Setup VCs
                String baseUrl = this.config.getString("address");
                String storageLocation = "trust/setup";
                this.setupService = new SetupService(
                    client,
                    did,
                    baseUrl,
                    vcService,
                    complianceService,
                    storageService,
                    storageLocation
                );

                // Trust Service for ServiceOffering VCs
                this.trustService = new TrustService(
                    client,
                    vertx,
                    did,
                    this.baseUri,
                    baseUrl,
                    storageService,
                    complianceService,
                    vcService,
                    resourcesService,
                    datasetsService
                );

            } catch (JOSEException e) {
                logger.error("Skipping JWK initialization as 'key' is not configured: " + e.getMessage());
            }
        } else {
            logger.debug("Config does not contain key");
        }

    }

    /*
    ###############################################################################################
    ############################################ DID ##############################################
    ###############################################################################################
    */

    /**
     * Handles HTTP requests to retrieve the Decentralized Identifier (DID) document.
     *
     * This method is an HTTP request handler that responds with the DID document in JSON-LD format.
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handleGetDID(RoutingContext context) {
        // Get params
        String did = context.request().getParam("did");

        // Create DID
        didService.getDID(did).onComplete(result -> {
            if (result.succeeded()) {
                // If the DID retrieved, respond with the DID document
                JsonObject didDocument = result.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(didDocument.toString());
            } else {
                // If DID not retrieved, respond with an error
                logger.error("Failed to create DID: " + result.cause());
                context.response().setStatusCode(500).end("Failed to create DID");
            }
        });
    }

    /**
     * Handles HTTP requests to create a Decentralized Identifier (DID) document
     *
     * @param context the routing context which provides access to the HTTP request and response objects
     */
    public void handleCreateDIDDocument(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String did = parameters.queryParameter("did").getString();

        // Create DID
        didService.createDID(did, "JWK2020-RSA", rsaPublicKey).onComplete(result -> {
            if (result.succeeded()) {
                // If the DID creation succeeded, respond with the DID document
                JsonObject didDocument = result.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(didDocument.toString());
            } else {
                // If the DID creation failed, respond with an error
                logger.error("Failed to create DID: " + result.cause());
                context.response().setStatusCode(500).end("Failed to create DID");
            }
        });
    }

    /*
    ###############################################################################################
    ######################################## SETUP VCs ###########################################
    ###############################################################################################
    */

    /**
     * Handles HTTP requests to initiate the setup process and retrieve the resulting compliance VC.
     *
     * This method is an HTTP request handler that triggers the setup process, which includes generating
     * various Verifiable Credentials (VCs), storing them, and submitting them for compliance verification.
     * It then responds with the compliance VC in JSON-LD format.
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handleGetSetup(RoutingContext context) {
        setupService.createSetup().onComplete(ar -> {
            if (ar.succeeded()) {
                // Future succeeded, send the response with the resulting JsonObject
                JsonObject complianceVC = ar.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(complianceVC.encodePrettily());
            } else {
                // Future failed, send an error response
                logger.error("Setup failed", ar.cause());
                context.response()
                        .setStatusCode(500)
                        .end("Failed to complete setup: " + ar.cause().getMessage());
            }
        });
    }

    /**
     * Handles HTTP requests to create and retrieve a
     * Legal Participant, Legal Registration Number, and Terms and Conditions Verifiable Credentials (VC).
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handleGetSetupVC(RoutingContext context) {
        String fileName = context.pathParam("fileName");
        setupService.getSetupVC(fileName).onComplete(ar -> {
            if (ar.succeeded()) {
                // Future succeeded, send the response with the resulting JsonObject
                JsonObject setupVC = ar.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(setupVC.encodePrettily());
            } else {
                // Future failed, send an error response
                logger.error("Getting Setup VC Failed", ar.cause());
                context.response()
                        .setStatusCode(500)
                        .end("Failed to complete getting core vc: " + ar.cause().getMessage());
            }
        });
    }

    /*
    ###############################################################################################
    ######################################## CREDENTIALS ##########################################
    ###############################################################################################
    */

    /**
     * Handles HTTP requests to retrieve a Verifiable Credential or Gaia-X Compliance Credential
     * associated with a specific resource.
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handleGetResourceVC(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String id = context.pathParam("id");
        String type = context.pathParam("type");
        boolean compliance = parameters.queryParameter("showCompliance").getBoolean();
        trustService.getResourceVC(id, type, compliance).onComplete(ar -> {
            if (ar.succeeded()) {
                // Future succeeded, send the response with the resulting JsonObject
                JsonObject setupVC = ar.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(setupVC.encodePrettily());
            } else {
                // Future failed, send an error response
                logger.error("Getting Resource VC Failed", ar.cause());
                context.response()
                        .setStatusCode(500)
                        .end("Failed to complete getting Resource VC: " + ar.cause().getMessage());
            }
        });
    }

    /*
    ###############################################################################################
    ######################################### RESOURCES ###########################################
    ###############################################################################################
    */

    /**
     * Handles HTTP requests to create and retrieve a Service Offering Verifiable Credential (VC).
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handleGetTrustByResource(RoutingContext context) {
        RequestParameters parameters = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        boolean compliance = parameters.queryParameter("showCompliance").getBoolean();
        String id = parameters.queryParameter("id").getString();
        String type = parameters.pathParameter("type").getString();

        trustService.getTrustByResource(compliance, type, id).onComplete(ar -> {
            if (ar.succeeded()) {
                // Future succeeded, send the response with the resulting JsonObject
                JsonObject resourceVC = ar.result();
                context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                        .end(resourceVC.encodePrettily());
            } else {
                // Future failed, send an error response
                logger.error("Getting Trust by Resource Failed", ar.cause());
                context.response()
                    .setStatusCode(500)
                    .end("Getting Trust by Resource Failed: " + ar.cause().getMessage());
            }
        });
    }

    /**
     * Handles HTTP requests to create and store Gaia-X Compliant Resources and
     * their corresponding Verifiable Credentials (VC).
     *
     * @param context the routing context which provides access to the HTTP request and response
     */
    public void handlePutTrustByResource(RoutingContext context) {
        String type = context.request().getParam("type");
        String verificationMethod = context.request().getParam("verificationMethod");
        String content = context.body().asString();
        String contentType = context.parsedHeaders().contentType().value();

        try {
            // Extract id from the content
            String id = extractIdFromContent(content);

            // Proceed with the trustService
            trustService.putTrustByResource(id, type, verificationMethod, content, contentType).onComplete(ar -> {
                if (ar.succeeded()) {
                    // Future succeeded, send the response with the resulting JsonObject
                    context.response().setStatusCode(201)
                            .putHeader(HttpHeaders.CONTENT_TYPE, RDFMimeTypes.JSONLD)
                            .end(new JsonObject().put("id", id).encode());
                } else {
                    // Future failed, handle the error response
                    logger.error("Putting Trust by Resource Failed", ar.cause());

                    // Try to parse the error response (if it contains a JSON error)
                    String errorMessage = ar.cause().getMessage();
                    try {
                        JsonObject errorJson = new JsonObject(errorMessage);
                        int statusCode = errorJson.getInteger("status", 500);
                        String errorDetail = errorJson.getString("error", "Unknown error");

                        // Set the appropriate status code and send the error message
                        context.response()
                                .setStatusCode(statusCode)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .end(new JsonObject().put("error", errorDetail).encodePrettily());
                    } catch (DecodeException e) {
                        // If the error message isn't a valid JSON, return a 500 Internal Server Error
                        context.response()
                                .setStatusCode(500)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "text/plain")
                                .end("Putting Trust by Resource Failed: " + errorMessage);
                    }
                }
            });
        } catch (IllegalArgumentException e) {
            // Handle missing or invalid ID error
            logger.error("Invalid content: " + e.getMessage());
            context.response()
                    .setStatusCode(400)
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .end(new JsonObject().put("error", e.getMessage()).encodePrettily());
        }
    }

    /*
    ###############################################################################################
    ######################################## GENERAL ##############################################
    ###############################################################################################
    */

    /**
     * Extracts the 'id' from the given JSON content string.
     *
     * @param content The JSON content as a string.
     * @return The extracted 'id'.
     * @throws IllegalArgumentException if the 'id' is missing or the content is invalid.
     */
    private String extractIdFromContent(String content) {
        try {
            JsonObject contentJson = new JsonObject(content);

            // Ensure the 'id' field exists
            String id = contentJson.getString("id");
            if (id == null || id.isEmpty()) {
                throw new IllegalArgumentException("Missing or empty 'id' in the content.");
            }
            return id;

        } catch (DecodeException e) {
            // If the content cannot be parsed into a valid JSON
            throw new IllegalArgumentException("Invalid JSON content: " + e.getMessage());
        }
    }

}
