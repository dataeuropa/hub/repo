/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.ext.trust.services;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ComplianceService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private String complianceUrl;
    private WebClient client;

    public ComplianceService(WebClient client) {
        this.client = client;
        this.complianceUrl = "https://compliance.lab.gaia-x.eu/v1-staging/api/credential-offers";
    }

    /*
    Returns the Gaia-X Compliance VC for the VCs provided
    */
    public Future<JsonObject> getComplianceVC(
            List<JsonObject> VCs,
            String vcId
    ) {
        // Create VP to submit to compliance
        JsonObject vp = new JsonObject();

        JsonArray context = new JsonArray();
        context.add("https://www.w3.org/2018/credentials/v1");
        vp.put("@context", context);
        vp.put("type", "VerifiablePresentation");

        JsonArray VCsArray = new JsonArray();
        for (JsonObject vc : VCs) {
            VCsArray.add(vc);
        }
        vp.put("verifiableCredential", VCsArray);

//        logger.info("VP: " + vp.toString());
//        String vpString = vp.toString();
//        int chunkSize = 1000; // Log in chunks of 1000 characters
//        for (int i = 0; i < vpString.length(); i += chunkSize) {
//            logger.info("VP Chunk: " + vpString.substring(i, Math.min(vpString.length(), i + chunkSize)));
//        }

        // Request Compliance VC
        Promise<JsonObject> promise = Promise.promise();
        String complianceUrl = this.complianceUrl + "?vcid=" + vcId;
        client.postAbs(complianceUrl)
                .putHeader("Content-Type", "application/json")
                .sendJsonObject(vp, ar -> {
                    if (ar.succeeded()) {
                        JsonObject complianceResponse = ar.result().bodyAsJsonObject();
                        logger.info("COMPLIANCE_RESPONSE: " + complianceResponse.toString());
                        if (complianceResponse.containsKey("error") && complianceResponse.containsKey("message")) {
                            promise.fail(complianceResponse.getString("message"));
                        } else {
                            promise.complete(complianceResponse);
                        }
                    } else {
                        promise.fail(ar.cause());
                    }
                });

        return promise.future();
    }
}
