/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.ext.trust.services;

import com.google.gson.JsonElement;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.resources.ResourcesService;
import io.piveau.rdf.Piveau;
import io.piveau.rdf.RDFMimeTypes;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.jena.rdf.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.HashSet;
import java.util.Set;
import io.vertx.ext.web.client.WebClient;

public class TrustService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private WebClient client;
    private Model mod = ModelFactory.createDefaultModel();
    private ResourcesService resourcesService;
    private DatasetsService datasetsService;
    Vertx vertx;
    String did;
    String baseUri;
    String baseUrl;
    ComplianceService complianceService;
    StorageService storageService;
    VCService vcService;

    public TrustService(
            WebClient client,
            Vertx vertx,
            String did,
            String baseUri,
            String baseUrl,
            StorageService storageService,
            ComplianceService complianceService,
            VCService vcService,
            ResourcesService resourcesService,
            DatasetsService datasetsService
    ) {
        this.client = client;
        this.vertx = vertx;
        this.did = did;
        this.baseUri = baseUri;
        this.baseUrl = baseUrl;
        this.storageService = storageService;
        this.complianceService = complianceService;
        this.vcService = vcService;
        this.resourcesService = resourcesService;
        this.datasetsService = datasetsService;
    }

    /**
     * Retrieves the Gaia-X Compliance Credentials for a given resource,
     * either from storage or generating the corresponding VCs.
     *
     * @param compliance Indicates whether to fetch compliance-related trust information.
     * @param type       The type of the resource.
     * @param id         The unique identifier for the resource.
     * @return           A Future containing the trust information for the resource as a JsonObject.
     */
    public Future<JsonObject> getTrustByResource(
            boolean compliance,
            String type,
            String id
    ) {
        Promise<JsonObject> promise = Promise.promise();

        String path = getStoragePathforServiceOffering(type, id);
        if (compliance) {
            path = getStoragePathforServiceOfferingCompliance(type, id);
        }

        Future<String> readFile = storageService.readFile(path);
        readFile.onComplete(resource -> {
            if (resource.succeeded()) {
                JsonObject res = new JsonObject(resource.result());
                promise.complete(res);
            } else {
                logger.error("Failed to read VC: " + resource.cause().getMessage());
                promise.fail(resource.cause());
            }
        });

        return promise.future();
    }

    /**
     * Stores a Gaia-X Compliant Resource.
     *
     * This method follows these steps:
     * 1. Creates a new Resource VC using the provided content and contentType.
     * 2. Checks if the VC is Gaia-X Compliant.
     * 3. If compliant, it stores the Resource and corresponding VCs.
     *
     * @param id          The unique identifier for the resource.
     * @param type        The type of the resource.
     * @param content     The content to be included in the VC.
     * @param contentType The type of content provided (e.g., application/json).
     * @return            A Future containing the updated trust information for the resource as a JsonObject.
     */
    public Future<JsonObject> putTrustByResource(
            String id,
            String type,
            String verificationMethod,
            String content,
            String contentType
    ) {
        Promise<JsonObject> promise = Promise.promise();

        logger.debug("*** Inside putTrustByResource ***");
        logger.debug("id: " + id);
        logger.debug("type: " + type);
        logger.debug("verification method: " + verificationMethod);

        if (verificationMethod == null || !verificationMethod.contains("#")) {
            throw new IllegalArgumentException("Invalid verificationMethod format");
        }
        String issuer = verificationMethod.split("#")[0];

        // Create Resource VC, Pass Compliance, Store Resource
        switch (type) {
            case "service-offering":
                logger.debug("Case: service-offering");
                Future<JsonObject> serviceOfferingVC = createServiceOfferingCompliance(id, type, issuer, verificationMethod, content, contentType);
                serviceOfferingVC.onComplete(ar -> {
                    if (ar.succeeded()) {
                        JsonObject resourceVC = ar.result();
                        promise.complete(resourceVC);
                    } else {
                        logger.error("Put service-offering resource failed: ", ar.cause());
                        promise.fail(ar.cause());
                    }
                });
                break;
            case "data-product":
                logger.debug("Case: data-product");
                Future<JsonObject> dataResourceVC = createDataProductCompliance(id, type, issuer, verificationMethod, content, contentType);
                dataResourceVC.onComplete(ar -> {
                    if (ar.succeeded()) {
                        JsonObject resourceVC = ar.result();
                        promise.complete(resourceVC);
                    } else {
                        logger.error("Put data-product resource failed: ", ar.cause());
                        promise.fail(ar.cause());
                    }
                });
                break;
            case "legal-participant":
                logger.debug("Case: legal-participant");
                Future<JsonObject> legalParticipantVC = createLegalParticipantCompliance(id, type, issuer, verificationMethod, content, contentType);
                legalParticipantVC.onComplete(ar -> {
                    if (ar.succeeded()) {
                        JsonObject resourceVC = ar.result();
                        promise.complete(resourceVC);
                    } else {
                        logger.error("Put legal-participant resource failed: ", ar.cause());
                        promise.fail(ar.cause());
                    }
                });
                break;
        }

        return promise.future();
    }

    /*
    ###############################################################################################
    ##################################### DATA PRODUCT ############################################
    ###############################################################################################
    */

    /**
     * Updates the JSON content by modifying the "@type" array.
     * Ensures that:
     * - "gx:ServiceOffering" is removed.
     * - "px:DataProduct" is added.
     * Maintains the integrity of the array and ensures no duplicates.
     *
     * @param content The JSON object to be updated.
     * @return The updated JSON object.
     */
    public static JsonObject updateDataProductType(JsonObject content) {
        // Check if the "@type" field exists and is a JsonArray
        if (content.containsKey("@type") && content.getValue("@type") instanceof JsonArray) {
            JsonArray typeArray = content.getJsonArray("@type");

            // Remove "gx:ServiceOffering" if it exists
            if (typeArray.contains("gx:ServiceOffering")) {
                typeArray.remove("gx:ServiceOffering");
            }

            // Add "px:DataProduct" if it doesn't already exist
            if (!typeArray.contains("px:DataProduct")) {
                typeArray.add("px:DataProduct");
            }
        } else {
            // If "@type" doesn't exist or isn't a JsonArray, create it and add "px:DataProduct"
            JsonArray typeArray = new JsonArray();
            typeArray.add("px:DataProduct");
            content.put("@type", typeArray);
        }

        return content;
    }

    /**
     * Creates a Service Offering Compliance Verifiable Credential (VC) for the given resource.
     *
     * This method performs the following steps:
     * 1. Creates a ServiceOffering VC document from the provided content.
     * 2. Signs the ServiceOffering VC document.
     * 3. Submits VCs to Gaia-X Clearing House for compliance check.
     * 4. If compliance passes, stores the ServiceOffering VC, Compliance VC, and the resource.
     *
     * @param resourceId   The ID of the resource.
     * @param type         The type of the resource.
     * @param content      The content of the resource in string format.
     * @param contentType  The content type of the resource.
     * @return             A Future that completes with the Compliance VC if successful.
     */
    private Future<JsonObject> createDataProductCompliance(String resourceId, String type, String issuer, String verificationMethod, String content, String contentType) {
        logger.debug("*** Inside createDataProductCompliance ***");
        logger.debug("Resource id: " + resourceId);
        logger.debug("type: " + type);
        logger.debug("issuer: " + issuer);
        logger.debug("verification method: " + verificationMethod);

        Promise<JsonObject> promise = Promise.promise();
        JsonObject contentJson = new JsonObject(content);
        JsonObject ids = extractIdsFromPayload(contentJson);
        JsonArray idLegalParticipants = ids.getJsonArray("idLegalParticipants");
        String idServiceOffering = ids.getString("idServiceOffering");
        String idDataResource = ids.getString("idDataResource");

        // List to hold futures for each legal participant's VCs retrieval
        List<Future> legalParticipantFutures = new ArrayList<>();

        // Loop through each legal participant ID and retrieve related VCs
        for (int i = 0; i < idLegalParticipants.size(); i++) {
            String idLegalParticipant = idLegalParticipants.getString(i);

            // Retrieve LegalParticipant VC
            String storagePathVCLegalParticipant = getStoragePathforLegalParticipant(idLegalParticipant);
            Future<String> futureVCLegalParticipant = storageService.readFile(storagePathVCLegalParticipant);

            // Retrieve LegalRegistrationNumber VC
            String storagePathVCLegalRegistrationNumber = getStoragePathforLegalRegistrationNumber(idLegalParticipant);
            Future<String> futureVCLegalRegistrationNumber = storageService.readFile(storagePathVCLegalRegistrationNumber);

            // Combine the futures for each legal participant's VCs into a single future
            Future<JsonObject> combinedFuture = CompositeFuture.all(futureVCLegalParticipant, futureVCLegalRegistrationNumber)
                    .map(compositeFuture -> {
                        // Create a JsonObject to store both VCs for this participant
                        JsonObject legalVCs = new JsonObject();
                        legalVCs.put("VCLegalParticipant", new JsonObject(futureVCLegalParticipant.result()));
                        legalVCs.put("VCLegalRegistrationNumber", new JsonObject(futureVCLegalRegistrationNumber.result()));
                        return legalVCs;
                    });

            // Add combined future to the list
            legalParticipantFutures.add(combinedFuture);
        }

        // Once all legal participant VCs have been retrieved, proceed with compliance
        CompositeFuture.all(legalParticipantFutures).onComplete(allLegalVCs -> {
            if (allLegalVCs.succeeded()) {
                List<JsonObject> VCs = new ArrayList<>();

                // Collect all VCs for each legal participant
                for (int i = 0; i < allLegalVCs.result().size(); i++) {
                    JsonObject legalVCs = (JsonObject) allLegalVCs.result().resultAt(i);
                    VCs.add(legalVCs.getJsonObject("VCLegalParticipant"));
                    VCs.add(legalVCs.getJsonObject("VCLegalRegistrationNumber"));
                }

                // Retrieve ServiceOffering and DataResource VCs
                Future<JsonObject> futureVCServiceOffering = getServiceOfferingVCFromPayload(contentJson, issuer, verificationMethod);
                Future<JsonObject> futureVCDataResource = getDataResourceVCFromPayload(contentJson, issuer, verificationMethod);

                // Once both ServiceOffering and DataResource VCs are available, submit them for compliance
                CompositeFuture.all(futureVCServiceOffering, futureVCDataResource).onComplete(compositeFuture2 -> {
                    if (compositeFuture2.succeeded()) {
                        // Parse ServiceOffering and DataResource VCs
                        JsonObject VCServiceOffering = futureVCServiceOffering.result();
                        JsonObject VCDataResource = futureVCDataResource.result();

                        // Add ServiceOffering and DataResource VCs to the VCs list
                        VCs.add(VCServiceOffering);
                        VCs.add(VCDataResource);

                        // Submit VCs to compliance service
                        String vcId = baseUrl + "/trust/" + type + "/" + resourceId + "?showCompliance=true";
                        Future<JsonObject> futureComplianceVC = complianceService.getComplianceVC(VCs, vcId);

                        // If compliance succeeds, store the various VCs and resources
                        futureComplianceVC.onSuccess(VCServiceOfferingCompliance -> {
                            // Store the ServiceOffering VC
                            storageService.writeFile(getStoragePathforServiceOffering(type, idServiceOffering), VCServiceOffering.toString())
                                    .onFailure(err -> logger.debug("Failed to write ServiceOffering VC: " + err.getMessage()));

                            // Store the DataResource VC
                            storageService.writeFile(getStoragePathforDataResource(type, idDataResource), VCDataResource.toString())
                                    .onFailure(err -> logger.debug("Failed to write DataResource VC: " + err.getMessage()));

                            // Store the Compliance VC
                            storageService.writeFile(getStoragePathforServiceOfferingCompliance(type, resourceId), VCServiceOfferingCompliance.toString())
                                    .onFailure(err -> logger.debug("Failed to write Compliance VC: " + err.getMessage()));

                            // Store the ServiceOffering Resource
                            JsonObject serviceOfferingContent = replaceIdWithAtId(new JsonObject(content));
                            serviceOfferingContent = updateDataProductType(serviceOfferingContent);
                            putResource(resourceId, type, serviceOfferingContent.toString(), contentType)
                                    .onComplete(putRes -> {
                                        if (putRes.succeeded()) {
                                            logger.debug("Successfully stored DataProduct resource.");
                                            promise.complete(VCServiceOfferingCompliance);
                                        } else {
                                            logger.debug("Failed to store DataProduct resource: " + putRes.cause().getMessage());
                                            promise.fail(createErrorResponse(500, "Failed to store ServiceOffering resource"));
                                        }
                                    });
                        }).onFailure(err -> {
                            logger.error("Failed to get Compliance VC: " + err.getMessage());
                            promise.fail(createErrorResponse(422, "ServiceOffering is not Gaia-X compliant: " + err.getMessage()));
                        });
                    } else {
                        logger.error("Failed to create ServiceOffering or DataResource VCs: " + compositeFuture2.cause().getMessage());
                        promise.fail(createErrorResponse(500, "Failed to create ServiceOffering or DataResource VCs"));
                    }
                });
            } else {
                logger.error("Failed to read LegalParticipant or LegalRegistrationNumber VCs: " + allLegalVCs.cause().getMessage());
                promise.fail(createErrorResponse(500, "Failed to read LegalParticipant or LegalRegistrationNumber VCs"));
            }
        });

        return promise.future();
    }

    /*
    ###############################################################################################
    ################################### SERVICE OFFERING ##########################################
    ###############################################################################################
    */


    /**
     * Creates a Service Offering Compliance Verifiable Credential (VC) for the given resource.
     *
     * This method performs the following steps:
     * 1. Creates a ServiceOffering VC document from the provided content.
     * 2. Signs the ServiceOffering VC document.
     * 3. Submits VCs to Gaia-X Clearing House for compliance check.
     * 4. If compliance passes, stores the ServiceOffering VC, Compliance VC, and the resource.
     *
     * @param resourceId   The ID of the resource.
     * @param type         The type of the resource.
     * @param content      The content of the resource in string format.
     * @param contentType  The content type of the resource.
     * @return             A Future that completes with the Compliance VC if successful.
     */
    private Future<JsonObject> createServiceOfferingCompliance(String resourceId, String type, String issuer, String verificationMethod, String content, String contentType) {
        logger.debug("*** Inside createServiceOfferingCompliance ***");
        logger.debug("Resource id: " + resourceId);
        logger.debug("type: " + type);
        logger.debug("issuer: " + issuer);
        logger.debug("verificationMethod: " + verificationMethod);

        Promise<JsonObject> promise = Promise.promise();
        JsonObject contentJson = new JsonObject(content);
        JsonObject ids = extractIdsFromPayload(contentJson);
        JsonArray idLegalParticipants = ids.getJsonArray("idLegalParticipants");
        String idServiceOffering = ids.getString("idServiceOffering");

        // List to hold futures for each legal participant's VCs retrieval
        List<Future> legalParticipantFutures = new ArrayList<>();

        // Loop through each legal participant ID and retrieve related VCs
        for (int i = 0; i < idLegalParticipants.size(); i++) {
            String idLegalParticipant = idLegalParticipants.getString(i);

            // Retrieve LegalParticipant VC
            String storagePathVCLegalParticipant = getStoragePathforLegalParticipant(idLegalParticipant);
            Future<String> futureVCLegalParticipant = storageService.readFile(storagePathVCLegalParticipant);

            // Retrieve LegalRegistrationNumber VC
            String storagePathVCLegalRegistrationNumber = getStoragePathforLegalRegistrationNumber(idLegalParticipant);
            Future<String> futureVCLegalRegistrationNumber = storageService.readFile(storagePathVCLegalRegistrationNumber);

            // Combine the futures for each legal participant's VCs into a single future
            Future<JsonObject> combinedFuture = CompositeFuture.all(futureVCLegalParticipant, futureVCLegalRegistrationNumber)
                    .map(compositeFuture -> {
                        // Create a JsonObject to store both VCs for this participant
                        JsonObject legalVCs = new JsonObject();
                        legalVCs.put("VCLegalParticipant", new JsonObject(futureVCLegalParticipant.result()));
                        legalVCs.put("VCLegalRegistrationNumber", new JsonObject(futureVCLegalRegistrationNumber.result()));
                        return legalVCs;
                    });

            // Add combined future to the list
            legalParticipantFutures.add(combinedFuture);
        }

        // Once all legal participant VCs have been retrieved, proceed with compliance
        CompositeFuture.all(legalParticipantFutures).onComplete(allLegalVCs -> {
            if (allLegalVCs.succeeded()) {
                List<JsonObject> VCs = new ArrayList<>();

                // Collect all VCs for each legal participant
                for (int i = 0; i < allLegalVCs.result().size(); i++) {
                    JsonObject legalVCs = (JsonObject) allLegalVCs.result().resultAt(i);
                    VCs.add(legalVCs.getJsonObject("VCLegalParticipant"));
                    VCs.add(legalVCs.getJsonObject("VCLegalRegistrationNumber"));
                }

                // Retrieve ServiceOffering VC
                Future<JsonObject> futureVCServiceOffering = getServiceOfferingVCFromPayload(contentJson, issuer, verificationMethod);

                // Once ServiceOffering is available, submit for compliance
                futureVCServiceOffering.onComplete(compositeFuture2 -> {
                    if (compositeFuture2.succeeded()) {
                        // Parse ServiceOffering VC
                        JsonObject VCServiceOffering = futureVCServiceOffering.result();

                        // Add ServiceOffering VC to the VCs list
                        VCs.add(VCServiceOffering);

                        // Submit VCs to compliance service
                        String vcId = baseUrl + "/trust/" + type + "/" + resourceId + "?showCompliance=true";
                        Future<JsonObject> futureComplianceVC = complianceService.getComplianceVC(VCs, vcId);

                        // If compliance succeeds, store the various VCs and resources
                        futureComplianceVC.onSuccess(VCServiceOfferingCompliance -> {
                            // Store the ServiceOffering VC
                            logger.debug("Storage path for service offering: " + getStoragePathforServiceOffering(type, resourceId));
                            storageService.writeFile(getStoragePathforServiceOffering(type, idServiceOffering), VCServiceOffering.toString())
                                    .onFailure(err -> logger.debug("Failed to write ServiceOffering VC: " + err.getMessage()));

                            // Store the Compliance VC
                            storageService.writeFile(getStoragePathforServiceOfferingCompliance(type, resourceId), VCServiceOfferingCompliance.toString())
                                    .onFailure(err -> logger.debug("Failed to write Compliance VC: " + err.getMessage()));

                            // Store the ServiceOffering Resource
                            JsonObject serviceOfferingContent = replaceIdWithAtId(new JsonObject(content));
                            putResource(resourceId, type, serviceOfferingContent.toString(), contentType)
                                    .onComplete(putRes -> {
                                        if (putRes.succeeded()) {
                                            logger.debug("Successfully stored ServiceOffering resource.");
                                            promise.complete(VCServiceOfferingCompliance); // Complete with the Service Offering Compliance VC
                                        } else {
                                            logger.debug("Failed to store ServiceOffering resource: " + putRes.cause().getMessage());
                                            promise.fail(createErrorResponse(500, "Failed to store ServiceOffering resource"));
                                        }
                                    });
                        }).onFailure(err -> {
                            logger.error("Failed to get Compliance VC: " + err.getMessage());
                            promise.fail(createErrorResponse(422, "ServiceOffering is not Gaia-X compliant: " + err.getMessage()));
                        });
                    } else {
                        logger.error("Failed to create ServiceOffering VC: " + compositeFuture2.cause().getMessage());
                        promise.fail(createErrorResponse(500, "Failed to create ServiceOffering VC"));
                    }
                });
            } else {
                logger.error("Failed to read LegalParticipant or LegalRegistrationNumber VCs: " + allLegalVCs.cause().getMessage());
                promise.fail(createErrorResponse(500, "Failed to read LegalParticipant or LegalRegistrationNumber VCs"));
            }
        });

        return promise.future();
    }

    /**
     * Processes a Service Offering Verifiable Credential (VC) from the provided payload.
     * This method performs the following steps:
     * - Extracts the Service Offering Credential Subject from the payload.
     * - Constructs the VC document based on the extracted credential subject.
     * - Signs the VC document and returns the signed VC.
     *
     * The method returns a Future that will complete with the signed Service Offering VC (as a JsonObject) or fail with an error.
     *
     * Error handling:
     * - If any errors occur during the signing process, the method logs the error and fails the promise with an appropriate error response.
     *
     * @param payload The JSON payload containing the Service Offering Credential information.
     * @return A Future that completes with the signed Service Offering VC (JsonObject) or fails if an error occurs.
     * @throws IllegalArgumentException If the payload is invalid or if required fields are missing.
     */
    private Future<JsonObject> getServiceOfferingVCFromPayload(JsonObject payload, String issuer, String verificationMethod) throws IllegalArgumentException {
        logger.debug("*** Inside getServiceOfferingVCFromPayload");

        Promise<JsonObject> promise = Promise.promise();
        payload = new JsonObject(payload.toString());

        // Get Credential Subject
        JsonObject credentialSubject = getServiceOfferingCredentialSubjectFromPayload(payload);

        // Get VC Document
        JsonObject document = getServiceOfferingVCDocument(credentialSubject, issuer);

        // Sign VC Document
        signVC(document, verificationMethod).onSuccess(serviceOfferingVC -> {
            logger.debug("Successfully signed ServiceOffering VC");
            promise.complete(serviceOfferingVC);
        }).onFailure(err -> {
            logger.error("Failed to create ServiceOffering VC: " + err.getMessage());
            promise.fail(createErrorResponse(500, "Failed to create ServiceOffering VC"));
        });

        return promise.future();
    }

    /**
     * Extracts and modifies the Service Offering Credential Subject from the provided payload.
     * This method performs several modifications:
     * - Removes the "gx" attribute from the "@context" list.
     * - Removes the "px:PossibleXServiceOfferingExtension" from the "@type" list.
     * - Removes the "px:assetId" and "px:providerUrl" attributes.
     * - Updates the "gx:aggregationOf" array by keeping only the "id" fields from the resources.
     *
     * If any errors occur (e.g., missing or invalid elements), the method logs the errors and
     * throws an IllegalArgumentException.
     *
     * @param payload The original JSON payload containing the Service Offering Credential.
     * @return The modified JSON object representing the Service Offering Credential Subject.
     * @throws IllegalArgumentException If any required fields are missing or invalid.
     */
    private JsonObject getServiceOfferingCredentialSubjectFromPayload(JsonObject payload) throws IllegalArgumentException {
        logger.debug("*** Inside getServiceOfferingCredentialSubjectFromPayload");
        logger.debug("Payload: " + payload.toString());

        // Create a copy of credentialSubject
        JsonObject credentialSubject = new JsonObject(payload.toString());

        // Remove px:PossibleXServiceOfferingExtension from @type list
        credentialSubject.put("@type", "gx:ServiceOffering");

        // Remove gx from context
        JsonObject context = credentialSubject.getJsonObject("@context");
        if (context != null) {
            context.remove("gx");
        } else {
            logger.warn("Context is missing in the payload. Proceeding without modification.");
        }

        // Remove px:assetId and px:providerUrl attributes
        credentialSubject.remove("px:assetId");
        credentialSubject.remove("px:providerUrl");

        // Handle gx:aggregationOf attribute
        JsonArray newResources = new JsonArray();
        JsonArray resources = credentialSubject.getJsonArray("gx:aggregationOf");

        if (resources != null && !resources.isEmpty()) {
            for (int i = 0; i < resources.size(); i++) {
                // Ensure the element is a JsonObject
                if (resources.getValue(i) instanceof JsonObject) {
                    JsonObject resource = resources.getJsonObject(i);
                    JsonObject newResource = new JsonObject();

                    // Ensure 'id' exists before trying to retrieve it
                    if (resource.containsKey("id")) {
                        newResource.put("id", resource.getValue("id"));
                        newResources.add(newResource);  // Add to new array
                    } else {
                        logger.warn("Resource at index " + i + " does not contain 'id'. Skipping this resource.");
                    }
                } else {
                    logger.warn("Invalid resource type at index " + i + ". Expected JsonObject but found: "
                            + (resources.getValue(i) != null ? resources.getValue(i).getClass().getName() : "null"));
                }
            }
        } else if (resources == null) {
            logger.warn("'gx:aggregationOf' is missing in the payload.");
        } else {
            logger.warn("'gx:aggregationOf' is empty.");
        }

        // Add the updated gx:aggregationOf to the credentialSubject
        credentialSubject.put("gx:aggregationOf", newResources);

        return credentialSubject;
    }

    /**
     * Constructs a Verifiable Credential (VC) document for a Service Offering.
     *
     * @param credentialSubject The JSON object representing the credential subject data.
     * @return                  A JsonObject representing the complete Service Offering VC document.
     */
    private JsonObject getServiceOfferingVCDocument(JsonObject credentialSubject, String issuer) {
        JsonObject document = new JsonObject();

        // Create the @context array
        JsonArray contextArray = new JsonArray();
        contextArray.add("https://www.w3.org/2018/credentials/v1");
        contextArray.add("https://w3id.org/security/suites/jws-2020/v1");
        contextArray.add("https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#");
        // NOTE: adding gx to the contextArray throws an error
        // contextArray.add(new JsonObject().put("gx", "https://w3id.org/gaia-x/development#"));

        // Build the final document
        document.put("@context", contextArray);
        document.put("type", "VerifiableCredential");
        document.put("id", "urn:uuid:" + UUID.randomUUID());
        document.put("issuer", issuer);
        String issuanceDate = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        document.put("issuanceDate", issuanceDate);
        document.put("credentialSubject", credentialSubject);

        return document;
    }

    private String getStoragePathforServiceOffering(String resource, String id) {
        return "trust/" + resource + "/" + id + ".json";
    }

    private String getStoragePathforServiceOfferingCompliance(String resource, String id) {
        return "trust/" + resource + "/compliance_" + id + ".json";
    }

    /*
    ###############################################################################################
    ##################################### DATA RESOURCE ###########################################
    ###############################################################################################
    */

    /**
     * Processes a Data Resource Verifiable Credential (VC) from the provided payload.
     * This method performs the following steps:
     * - Extracts the Data Resource Credential Subject from the payload.
     * - Constructs the VC document from the credential subject.
     * - Signs the VC document.
     *
     * The method returns a Future that will complete with the signed VC (as a JsonObject) or fail with an error.
     *
     * Error handling:
     * - If any errors occur during the signing process, the method logs the error and fails the promise with an appropriate error response.
     *
     * @param payload The JSON payload containing the Data Resource Credential information.
     * @return A Future that completes with the signed Data Resource VC (JsonObject) or fails if an error occurs.
     * @throws IllegalArgumentException If the payload is invalid or if required fields are missing.
     */
    private Future<JsonObject> getDataResourceVCFromPayload(JsonObject payload, String issuer, String verificationMethod) throws IllegalArgumentException {
        logger.debug("*** Inside getDataResourceVCFromPayload");
        logger.debug("Payload: " + payload);

        Promise<JsonObject> promise = Promise.promise();
        payload = new JsonObject(payload.toString());

        // Get Credential Subject
        JsonObject credentialSubject = getDataResourceCredentialSubjectFromPayload(payload);

        // Get VC Document
        JsonObject document = getDataResourceVCDocument(credentialSubject, issuer);

        // Sign VC Document
        signVC(document, verificationMethod).onSuccess(vc -> {
            logger.debug("Successfully signed DataResource VC");
            promise.complete(vc);
        }).onFailure(err -> {
            logger.error("Failed to create DataResource VC: " + err.getMessage());
            promise.fail(createErrorResponse(500, "Failed to create DataResource VC"));
        });

        return promise.future();
    }

    /**
     * Extracts and validates the Data Resource Credential Subject from the provided payload.
     * This method performs the following actions:
     * - Checks if the "gx:aggregationOf" element exists in the payload.
     * - Ensures the "gx:aggregationOf" array is not empty and contains a valid JsonObject.
     * - If valid, the first resource in the "gx:aggregationOf" array is returned as the credential subject.
     *
     * If any errors occur (e.g., missing or invalid elements), the method logs the errors and
     * throws an IllegalArgumentException.
     *
     * @param payload The original JSON payload containing the Data Resource Credential.
     * @return The extracted Data Resource Credential Subject as a JsonObject.
     * @throws IllegalArgumentException If any required fields are missing or invalid.
     */
    private JsonObject getDataResourceCredentialSubjectFromPayload(JsonObject payload) throws IllegalArgumentException {
        // Create copy of credentialSubject
        payload = new JsonObject(payload.toString());

        // Data Resource Credential Subject is the gx:aggregationOf element
        JsonObject credentialSubject = new JsonObject();
        JsonArray resources = payload.getJsonArray("gx:aggregationOf");
        // Error handling: Check if resources is null or empty
        if (resources == null) {
            logger.error("'gx:aggregationOf' is missing or null in the payload.");
            throw new IllegalArgumentException("'gx:aggregationOf' is missing or null in the payload.");
        } else if (resources.isEmpty()) {
            logger.error("'gx:aggregationOf' is empty.");
            throw new IllegalArgumentException("'gx:aggregationOf' is empty.");
        } else {
            // Check if the first element is a JsonObject
            if (resources.getValue(0) instanceof JsonObject) {
                credentialSubject = resources.getJsonObject(0);
            } else {
                String errorMessage = "First element in 'gx:aggregationOf' is not a JsonObject. Found: " + resources.getValue(0).getClass().getName();
                logger.error(errorMessage);
                throw new IllegalArgumentException(errorMessage);
            }
        }

        // Add @context
        try {
            // Check if payload contains @context and is a JsonObject
            if (payload.containsKey("@context") && payload.getValue("@context") instanceof JsonObject) {
                JsonObject context = payload.getJsonObject("@context");
                context.remove("gx");
                credentialSubject.put("@context", context);
            } else {
                String errorMessage = "Error: @context is either missing or not a JsonObject in the payload.";
                logger.error(errorMessage);
                throw new IllegalArgumentException(errorMessage);
            }
        } catch (Exception e) {
            String errorMessage = "An unexpected error occurred: " + e.getMessage();
            logger.error(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }
        credentialSubject.put("@context", payload.getJsonObject("@context"));

        return credentialSubject;
    }

    /**
     * Constructs a Verifiable Credential (VC) document for a Service Offering.
     *
     * @param credentialSubject The JSON object representing the credential subject data.
     * @return                  A JsonObject representing the complete Service Offering VC document.
     */
    private JsonObject getDataResourceVCDocument(JsonObject credentialSubject, String issuer) {
        JsonObject document = new JsonObject();

        // Create the @context array
        JsonArray contextArray = new JsonArray();
        contextArray.add("https://www.w3.org/2018/credentials/v1");
        contextArray.add("https://w3id.org/security/suites/jws-2020/v1");
        contextArray.add("https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#");

        // Make sure type is "gx:DataResource"
        if (credentialSubject.containsKey("type")) {
            credentialSubject.put("type", "gx:DataResource");
        }
        if (credentialSubject.containsKey("@type")) {
            credentialSubject.put("@type", "gx:DataResource");
        }

        // Build the final document
        document.put("@context", contextArray);
        document.put("id", "urn:uuid:" + UUID.randomUUID());
        document.put("type", "VerifiableCredential");
        document.put("credentialSubject", credentialSubject);
        document.put("issuer", issuer);

        String issuanceDate = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        document.put("issuanceDate", issuanceDate);

        return document;
    }

    private String getStoragePathforDataResource(String type, String id) {
        return "trust/data-resource/" + id + ".json";
    }

    private String getStoragePathforDataResourceCompliance(String type, String id) {
        return "trust/data-resource/compliance_" + id + ".json";
    }

    /*
    ###############################################################################################
    ################################## LEGAL PARTICIPANT ##########################################
    ###############################################################################################
    */

    /**
     * Creates a Legal Participant Compliance Verifiable Credential (VC) for the given resource.
     *
     * This method performs the following steps:
     * 1. Creates a LegalParticipant VC document from the provided content.
     * 2. Signs the LegalParticipant VC document.
     * 3. Submits VCs to Gaia-X Clearing House for compliance check.
     * 4. If compliance passes, stores the LegalParticipant VC, Compliance VC, and the resource.
     *
     * @param resourceId   The ID of the resource.
     * @param type         The type of the resource.
     * @param content      The content of the resource in string format.
     * @param contentType  The content type of the resource.
     * @return             A Future that completes with the Compliance VC if successful.
     */
    private Future<JsonObject> createLegalParticipantCompliance(String resourceId, String type, String issuer, String verificationMethod, String content, String contentType) {
        Promise<JsonObject> promise = Promise.promise();
        logger.debug("*** Inside createLegalParticipantCompliance ***");
        logger.debug("Resource id: " + resourceId);
        logger.debug("type: " + type);
        logger.debug("issuer: " + issuer);
        logger.debug("verificationMethod: " + verificationMethod);

        // Create LegalParticipant CredentialSubject
        JsonObject credentialSubject = getCredentialSubjectFromLegalParticipant(new JsonObject(content));
        logger.debug("Credential Subject: " + credentialSubject);

        getVCLegalRegistrationNumber(credentialSubject).onSuccess(VCLegalRegistrationNumber -> {
            // Get LegalRegistrationNumber VC Id
            String idLegalRegistrationNumber = null;
            if (VCLegalRegistrationNumber.containsKey("id") && VCLegalRegistrationNumber.getValue("id") != null) {
                idLegalRegistrationNumber = VCLegalRegistrationNumber.getString("id");
            } else {
                logger.warn("The Legal Registration Number VC 'id' attribute is missing or null.");
            }

            // Create and Sign LegalParticipant VC Document
            JsonObject document = getLegalParticipantVCDocument(credentialSubject, issuer, idLegalRegistrationNumber);
            signVC(document, verificationMethod)
                .onSuccess(VCLegalParticipant -> {
                    logger.debug("VCLegalParticipant: " + VCLegalParticipant);
                    logger.debug("VCLegalRegistrationNumber: " + VCLegalRegistrationNumber);

                    // Submit to compliance
                    List<JsonObject> VCs = new ArrayList<>();
                    VCs.add(VCLegalParticipant);
                    VCs.add(VCLegalRegistrationNumber);
                    String vcId = baseUrl + "/trust/" + type + "/" + resourceId + "?showCompliance=true";

                    Future<JsonObject> futureComplianceVC = complianceService.getComplianceVC(VCs, vcId);

                    // If passes compliance, store LegalParticipant VC, Compliance VC, and Resource
                    futureComplianceVC.onSuccess(VCLegalParticipantCompliance -> {
                        // Store the LegalRegistrationNumber VC
                        storageService.writeFile(getStoragePathforLegalRegistrationNumber(resourceId), VCLegalRegistrationNumber.toString())
                                .onComplete(writeComplianceRes -> {
                                    if (writeComplianceRes.succeeded()) {
                                        logger.debug("Successfully stored LegalRegistrationNumber VC.");
                                    } else {
                                        logger.debug("Failed to write LegalRegistrationNumber VC: " + writeComplianceRes.cause().getMessage());
                                        promise.fail(createErrorResponse(500, "Failed to store LegalRegistrationNumber VC"));
                                    }
                                });

                        // Store the LegalParticipant VC
                        logger.debug("Storage path for legal participant: " + getStoragePathforLegalParticipant(resourceId));
                        storageService.writeFile(getStoragePathforLegalParticipant(resourceId), VCLegalParticipant.toString())
                                .onComplete(writeComplianceRes -> {
                                    if (writeComplianceRes.succeeded()) {
                                        logger.debug("Successfully stored LegalParticipant VC.");
                                    } else {
                                        logger.debug("Failed to write LegalParticipant VC: " + writeComplianceRes.cause().getMessage());
                                        promise.fail(createErrorResponse(500, "Failed to store LegalParticipant VC"));
                                    }
                                });

                        // Store the Compliance VC
                        storageService.writeFile(getStoragePathforLegalParticipantCompliance(resourceId), VCLegalParticipantCompliance.toString())
                                .onComplete(writeComplianceRes -> {
                                    if (writeComplianceRes.succeeded()) {
                                        logger.debug("Successfully stored LegalParticipant Compliance VC.");
                                    } else {
                                        logger.debug("Failed to write LegalParticipant Compliance VC: " + writeComplianceRes.cause().getMessage());
                                        promise.fail(createErrorResponse(500, "Failed to store Compliance VC"));
                                    }
                                });

                        // Store the LegalParticipant Resource
                        JsonObject legalParticipantContent = replaceIdWithAtId(new JsonObject(content));
                        putResource(resourceId, type, legalParticipantContent.toString(), contentType)
                                .onComplete(putRes -> {
                                    if (putRes.succeeded()) {
                                        logger.debug("Successfully stored LegalParticipant resource");
                                        promise.complete(VCLegalParticipantCompliance);  // Complete with the Legal Participant Compliance VC
                                    } else {
                                        logger.debug("Failed to store LegalParticipant resource: " + putRes.cause().getMessage());
                                        promise.fail(createErrorResponse(500, "Failed to store LegalParticipant resource"));
                                    }
                                });
                    }).onFailure(err -> {
                        logger.error("Failed to get Compliance VC: " + err.getMessage());
                        promise.fail(createErrorResponse(422, "LegalParticipant is not Gaia-X compliant: " + err.getMessage()));
                    });
                })
                .onFailure(err -> {
                    logger.error("Failed to sign LegalParticipant VC: " + err.getMessage());
                    promise.fail(createErrorResponse(500, "Failed to sign LegalParticipant VC: " + err.getMessage()));
                });
        })
        .onFailure(err -> {
            logger.error("Failed to create LegalParticipant VC: " + err.getMessage());
            promise.fail(createErrorResponse(422, "Failed to create LegalParticipant VC: " + err.getMessage()));
        });

        return promise.future();
    }

    private JsonObject getCredentialSubjectFromLegalParticipant(JsonObject credentialSubject) {
        // Remove px:PossibleXServiceOfferingExtension from @type list
        JsonArray typeArray = credentialSubject.getJsonArray("@type");
        if (typeArray != null) {
            typeArray.remove("px:PossibleXLegalParticipantExtension");
        }

        // Remove gx attribute from @context list
        JsonObject contextObject = credentialSubject.getJsonObject("@context");
        if (contextObject != null) {
            contextObject.remove("gx");
        }

        return credentialSubject;
    }

    /**
     * Constructs a Verifiable Credential (VC) document for a Legal Participant.
     *
     * @param credentialSubject The JSON object representing the credential subject data.
     * @return                  A JsonObject representing the complete Legal Participant VC document.
     */
    private JsonObject getLegalParticipantVCDocument(JsonObject credentialSubject, String issuer, String idLegalRegistrationNumber) {
        logger.debug("*** Inside getLegalParticipantVCDocument");

        JsonObject document = new JsonObject();

        // Create the @context array
        JsonArray contextArray = new JsonArray();
        contextArray.add("https://www.w3.org/2018/credentials/v1");
        contextArray.add("https://w3id.org/security/suites/jws-2020/v1");
        contextArray.add("https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#");

        // Update the credentialSubject to reference the legal registration number id
        JsonObject lrn = credentialSubject.getJsonObject("gx:legalRegistrationNumber");
        lrn.put("id", idLegalRegistrationNumber);
        credentialSubject.put("gx:legalRegistrationNumber", lrn);

        // Build the final document
        document.put("@context", contextArray);
        document.put("type", "VerifiableCredential");
        document.put("id", "urn:uuid:" + UUID.randomUUID());
        document.put("issuer", issuer);
        String issuanceDate = ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        document.put("issuanceDate", issuanceDate);
        document.put("credentialSubject", credentialSubject);

        logger.debug("Legal Participant VC Document: " + document);

        return document;
    }

    /**
     * Creates and returns the Gaia-X Legal Registration Number Verifiable Credential (VC).
     *
     * This method creates a Legal Registration Number request, sends the request to the Gaia-X Notary, and stores
     * the resulting Legal Registration Number VC.
     *
     * @return JsonObject representing the Gaia-X Legal Registration Number VC.
     */
    public Future<JsonObject> getVCLegalRegistrationNumber(JsonObject legalParticipantCredentialSubject) {
        logger.debug("*** Inside getVCLegalRegistrationNumber");
        logger.debug("Legal Participant Credential Subject: " + legalParticipantCredentialSubject);

        // Render request template
        JsonObject template = new JsonObject();

        JsonArray context = new JsonArray();
        context.add("https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant");
        template.put("@context", context);

        template.put("type", "gx:legalRegistrationNumber");
        // String lpId = legalParticipantCredentialSubject.getString("id");
        // String csId = baseUrl + "/trust/legal-registration-number/" + lpId + ".json#cs";
        String csId = "urn:uuid:" + UUID.randomUUID();
        template.put("id", csId);
        if (legalParticipantCredentialSubject.getJsonObject("gx:legalRegistrationNumber") != null) {
            JsonObject legalRegistrationNumber = legalParticipantCredentialSubject.getJsonObject("gx:legalRegistrationNumber");
            if (legalRegistrationNumber.containsKey("gx:vatID")) {
                template.put("gx:vatID", legalRegistrationNumber.getString("gx:vatID"));
            } else if (legalRegistrationNumber.containsKey("gx:leiCode")) {
                template.put("gx:leiCode", legalRegistrationNumber.getString("gx:leiCode"));
            } else if (legalRegistrationNumber.containsKey("gx:EORI")) {
                template.put("gx:EORI", legalRegistrationNumber.getString("gx:EORI"));
            }
        }

        logger.debug("Template: " + template);

        // Request LRN VC
        Promise<JsonObject> promise = Promise.promise();
        String notaryUrl = "https://registrationnumber.notary.lab.gaia-x.eu/v1/registrationNumberVC";
        String lrnVcId = "urn:uuid:" + UUID.randomUUID();
        client.postAbs(notaryUrl + "?vcid=" + lrnVcId)
            .putHeader("Content-Type", "application/json")
            .sendJsonObject(template, ar -> {
                if (ar.succeeded()) {
                    String responseBody = ar.result().bodyAsString();
                    try {
                        // Try parsing the response body as a JsonObject
                        JsonObject lrnVC = new JsonObject(responseBody);
                        promise.complete(lrnVC);
                    } catch (DecodeException e) {
                        // If it's not a valid JSON, assume it's an error string
                        logger.error("Received an error response: " + responseBody);
                        promise.fail("Legal Registration Number is not Gaia-X Compliant: " + responseBody.replace("\"", ""));
                    }
                } else {
                    logger.debug("*** Unable to get LegalRegistrationNumber from Notary: " + ar.cause());
                    promise.fail(ar.cause());
                }
            });

        return promise.future();
    }

    private String getStoragePathforLegalParticipant(String id) {
        return "trust/legal-participant/" + id + ".json";
    }

    private String getStoragePathforLegalParticipantCompliance(String id) {
        return "trust/legal-participant/compliance_" + id + ".json";
    }

    private String getStoragePathforLegalRegistrationNumber(String id) {
        return "trust/legal-registration-number/" + id + ".json";
    }

    /*
    ###############################################################################################
    ####################################### CREDENTIALS ###########################################
    ###############################################################################################
    */

    /**
     * Retrieves a Verifiable Credential (VC) JSON object for a specified resource.
     *
     * This method attempts to read a stored VC file for a resource based on its unique ID and type.
     * If the `compliance` flag is set to true, it will read the compliance VC file (with "compliance_" prefix).
     * Otherwise, it reads the standard VC file for the resource.
     *
     * @param id          The unique identifier of the resource.
     * @param type        The type of the resource (e.g., "serviceOffering").
     * @param compliance  A boolean flag indicating whether to retrieve the compliance VC (true)
     *                    or the standard VC (false).
     * @return            A Future that completes with the JSON object of the resource's VC if successful,
     *                    or fails if the file cannot be read.
     */
    public Future<JsonObject> getResourceVC(String id, String type, boolean compliance) {
        Promise<JsonObject> promise = Promise.promise();

        String filePath = "trust/" + type + "/" + id + ".json";
        if (compliance) {
            filePath = "trust/" + type + "/compliance_" + id + ".json";
        }

        Future<String> readVC = storageService.readFile(filePath);
        readVC.onComplete(readResult -> {
            if (readResult.succeeded()) {
                logger.debug("Reading Resource VC succeeded");
                promise.complete(new JsonObject(readResult.result()));
            } else {
                logger.debug("Failed to read Resource VC: " + readResult.cause().getMessage());
                promise.fail(readResult.cause());
            }
        });

        return promise.future();
    }

    /*
    ###############################################################################################
    ######################################## GENERAL ##############################################
    ###############################################################################################
    */

    /**
     * Extracts the required IDs from the provided JSON payload.
     *
     * @return A JsonObject containing the extracted IDs: idLegalParticipant, idServiceOffering, and idDataResource.
     * @throws IllegalArgumentException If the required fields are missing or invalid.
     */
    /**
     * Extracts the required IDs from the provided JSON payload.
     *
     * @return A JsonObject containing the extracted IDs: idLegalParticipants, idServiceOffering, and optionally idDataResource.
     * @throws IllegalArgumentException If the required fields are missing or invalid.
     */
    public JsonObject extractIdsFromPayload(JsonObject payload) throws IllegalArgumentException {
        JsonObject result = new JsonObject();
        Set<String> idLegalParticipantsSet = new HashSet<>();

        // Extract idLegalParticipant1 (gx:providedBy -> id)
        if (payload.containsKey("gx:providedBy") && payload.getJsonObject("gx:providedBy").containsKey("id")) {
            String idLegalParticipant = payload.getJsonObject("gx:providedBy").getString("id");
            idLegalParticipantsSet.add(idLegalParticipant);
        } else {
            logger.warn("Missing id in gx:providedBy");
        }

        // Check if gx:aggregationOf exists and process it if present
        if (payload.containsKey("gx:aggregationOf")) {
            JsonArray aggregationOf = payload.getJsonArray("gx:aggregationOf");

            if (!aggregationOf.isEmpty()) {
                JsonObject firstAggregation = aggregationOf.getJsonObject(0);

                // Safely extract gx:producedBy -> id's
                if (firstAggregation.containsKey("gx:producedBy")) {
                    Object producedBy = firstAggregation.getValue("gx:producedBy");

                    // One id
                    if (producedBy instanceof JsonObject) {
                        JsonObject producedByObj = (JsonObject) producedBy;
                        if (producedByObj.containsKey("id")) {
                            String idLegalParticipant3 = producedByObj.getString("id");
                            idLegalParticipantsSet.add(idLegalParticipant3);
                        }
                    }

                    // Array of id's
                    else if (producedBy instanceof JsonArray) {
                        JsonArray producedByArray = (JsonArray) producedBy;
                        for (int i = 0; i < producedByArray.size(); i++) {
                            JsonObject producedObj = producedByArray.getJsonObject(i);
                            if (producedObj.containsKey("id")) {
                                String idLegalParticipant3 = producedObj.getString("id");
                                idLegalParticipantsSet.add(idLegalParticipant3);
                            }
                        }
                    }
                    // Invalid type
                    else {
                        logger.error("gx:producedBy exists but is neither a JsonObject nor a JsonArray.");
                    }
                } else {
                    logger.warn("First aggregation does not contain gx:producedBy.");
                }

                // Safely extract id from first aggregation object
                if (firstAggregation.containsKey("id")) {
                    String idDataResource = firstAggregation.getString("id");
                    result.put("idDataResource", idDataResource);
                } else {
                    logger.warn("First aggregation does not contain id.");
                }
            } else {
                logger.warn("gx:aggregationOf is empty.");
            }
        } else {
            logger.warn("gx:aggregationOf is missing.");
        }

        // Convert the unique set of IDs to a JsonArray
        JsonArray idLegalParticipants = new JsonArray();
        idLegalParticipantsSet.forEach(idLegalParticipants::add);
        result.put("idLegalParticipants", idLegalParticipants);

        // Extract idServiceOffering (id attribute)
        if (payload.containsKey("id")) {
            String idServiceOffering = payload.getString("id");
            result.put("idServiceOffering", idServiceOffering);
        } else {
            throw new IllegalArgumentException("Missing id for Service Offering");
        }

        return result;
    }

    public static JsonObject replaceIdWithAtId(JsonObject jsonObject) {
        // Create a new JsonObject to store the transformed data
        JsonObject transformed = new JsonObject();

        // Iterate over all key-value pairs in the original JsonObject
        for (String key : jsonObject.fieldNames()) {
            Object value = jsonObject.getValue(key);

            // If value is another JsonObject, recurse
            if (value instanceof JsonObject) {
                value = replaceIdWithAtId((JsonObject) value);
            }
            // If value is a JsonArray, recurse on each element
            else if (value instanceof JsonArray) {
                value = replaceIdWithAtIdInArray((JsonArray) value);
            }

            // Replace "id" with "@id", keep other keys as they are
            if (key.equals("id")) {
                transformed.put("@id", value);
            } else {
                transformed.put(key, value);
            }
        }

        return transformed;
    }

    // Helper function to handle JsonArrays
    private static JsonArray replaceIdWithAtIdInArray(JsonArray jsonArray) {
        JsonArray transformedArray = new JsonArray();
        for (Object element : jsonArray) {
            if (element instanceof JsonObject) {
                transformedArray.add(replaceIdWithAtId((JsonObject) element));
            } else if (element instanceof JsonArray) {
                transformedArray.add(replaceIdWithAtIdInArray((JsonArray) element));
            } else {
                transformedArray.add(element); // Primitive types, add as-is
            }
        }
        return transformedArray;
    }

    /**
     * Signs a VC Document
     *
     * @return JsonObject representing the Gaia-X ServiceOffering VC.
     */
    public Future<JsonObject> signVC(JsonObject rawServiceOffering, String verificationMethod) {
        Promise<JsonObject> promise = Promise.promise();
        // Sign VC document
        vcService.signVCDocument(rawServiceOffering, verificationMethod)
                .onSuccess(vc -> {
                    promise.complete(vc);
                })
                .onFailure(err -> {
                    logger.error("Failed to sign VC Document");
                    promise.fail(createErrorResponse(500, "Failed to sign VC Document"));
                });

        // Return the future which will be completed or failed
        return promise.future();
    }

    private RuntimeException createErrorResponse(int statusCode, String message) {
        return new RuntimeException(new JsonObject()
            .put("status", statusCode)
            .put("error", message)
            .encode());
    }

    private Future<String> putResource(String id, String type, String content, String contentType) {
        return resourcesService.putResource(id, type, content, contentType);
    }
}
