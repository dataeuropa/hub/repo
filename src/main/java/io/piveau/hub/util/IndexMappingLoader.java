/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.util;

import io.piveau.hub.indexing.Indexing;
import io.piveau.profile.*;
import io.piveau.rdf.Piveau;
import io.piveau.vocabularies.vocabulary.DCATAP;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.*;

import static io.piveau.profile.Indexer.produceIndexInstructionsFromResource;

public class IndexMappingLoader {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Vertx vertx;
    private final JsonObject config;
    private final PiveauProfile piveauProfile;

    private final String defaultShape = "shapes/dcat-ap.hub.shapes.ttl";

    public IndexMappingLoader(Vertx vertx, JsonObject config, PiveauProfile piveauProfile) {
        this.vertx = vertx;
        this.config = config;
        this.piveauProfile = piveauProfile;
    }
    public Future<Void> load() {
        Promise<Void> promise = Promise.promise();

        AtomicReference<JsonObject> datasetInstr = new AtomicReference<>();
        AtomicReference<JsonObject> catalogInstr = new AtomicReference<>();
        findShapes().onSuccess(defaulModel -> {
            if (piveauProfile != null) {
                if (piveauProfile.datasetIsSet()) {
                    datasetInstr.set(produceIndexInstructionsFromResource(piveauProfile.dataset.getShaclShape().getResource(piveauProfile.dataset.getShapeUri())));
                    log.info("Loaded dataset schema from profile");
                } else {
                    datasetInstr.set(produceIndexInstructionsFromResource(defaulModel.getResource(DCATAP.DatasetShape.getURI())));
                    log.info("Loaded dataset schema from config folder");
                }
                if (piveauProfile.catalogIsSet()) {
                    catalogInstr.set(produceIndexInstructionsFromResource(piveauProfile.catalog.getShaclShape().getResource(piveauProfile.catalog.getShapeUri())));
                    log.info("Loaded catalog schema from profile");
                } else {
                    catalogInstr.set(produceIndexInstructionsFromResource(defaulModel.getResource(DCATAP.CatalogShape.getURI())));
                    log.info("Loaded catalog schema from config folder");
                }
            } else {
                datasetInstr.set(produceIndexInstructionsFromResource(defaulModel.getResource(DCATAP.DatasetShape.getURI())));
                log.info("Loaded dataset schema from config folder");
                catalogInstr.set(produceIndexInstructionsFromResource(defaulModel.getResource(DCATAP.CatalogShape.getURI())));
                log.info("Loaded catalog schema from config folder");
            }
            Indexing.init(catalogInstr.get(), datasetInstr.get());
            promise.complete();
        }).onFailure(error -> promise.fail(error.getMessage()));
        return promise.future();
    }

    private Future<Model> findShapes() {
        Promise<Model> promise = Promise.promise();
        vertx.fileSystem().readFile("conf/shapes/dcat-ap.hub.shapes.ttl")
                .onSuccess(file -> {
                    log.info("Loaded overwrite schema");
                    Model catModel = Piveau.toModel(file.getBytes(), Lang.TURTLE);
                    promise.complete(catModel);
                })
                .onFailure(ar -> vertx.fileSystem().readFile("shapes/dcat-ap.hub.shapes.ttl").onSuccess(file -> {
                    Model catModel = Piveau.toModel(file.getBytes(), Lang.TURTLE);
                    promise.complete(catModel);
                }).onFailure(promise::fail));
        return promise.future();
    }
}

