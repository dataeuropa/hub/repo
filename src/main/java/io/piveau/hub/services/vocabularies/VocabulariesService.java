package io.piveau.hub.services.vocabularies;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@ProxyGen
public interface VocabulariesService {
    String SERVICE_ADDRESS = "io.piveau.hub.vocabularies.queue";

    static VocabulariesService create(Vertx vertx, TripleStore tripleStore, JsonObject config, Handler<AsyncResult<VocabulariesService>> readyHandler) {
        return new VocabulariesServiceImpl(vertx, tripleStore, config, readyHandler);
    }

    static VocabulariesService createProxy(Vertx vertx, String address) {
        DeliveryOptions options = new DeliveryOptions().setSendTimeout(300000);
        return new VocabulariesServiceVertxEBProxy(vertx, address, options);
    }

    Future<String> listVocabularies(String acceptType, String valueType, Integer limit, Integer offset);

    Future<String> getVocabulary(String vocabularyId, String acceptType);

    Future<JsonArray> indexVocabulary(String vocabularyId);

    Future<String> putVocabulary(String vocabularyId, String vocabularyUri, String contentType, String content);

    Future<String> putVocabularyFile(String vocabularyId, String vocabularyUri, String contentType, String file);

    Future<String> putVocabularyChunk(String vocabularyId, String vocabularyUri, String contentType,
                                                 String chunk, String hash, int chunkId, int numberOfChunks);

    Future<Void> deleteVocabulary(String vocabularyId);
}
