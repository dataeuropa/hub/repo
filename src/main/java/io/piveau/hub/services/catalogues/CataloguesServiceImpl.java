package io.piveau.hub.services.catalogues;

import com.google.common.collect.Lists;
import io.piveau.HubRepo;
import io.piveau.dcatap.CatalogueManager;
import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.TripleStore;
import io.piveau.hub.Constants;
import io.piveau.hub.Defaults;
import io.piveau.hub.indexing.Indexing;
import io.piveau.hub.security.KeyCloakService;
import io.piveau.hub.services.datasets.DatasetsService;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.util.CatalogueHelper;
import io.piveau.json.ConfigHelper;
import io.piveau.rdf.Piveau;
import io.piveau.vocabularies.vocabulary.EDP;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RiotException;
import org.apache.jena.vocabulary.DCAT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class CataloguesServiceImpl implements CataloguesService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final TripleStore tripleStore;
    private final CatalogueManager catalogueManager;

    private IndexService indexService;

    private final DatasetsService datasetsService;

    private final KeyCloakService keycloakService;

    CataloguesServiceImpl(TripleStore tripleStore, Vertx vertx, JsonObject config, Handler<AsyncResult<CataloguesService>> readyHandler) {
        this.tripleStore = tripleStore;
        catalogueManager = tripleStore.getCatalogueManager();

        JsonObject indexConfig = ConfigHelper.forConfig(config).forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_SERVICE);

        Boolean searchServiceEnabled = indexConfig.getBoolean("enabled", Defaults.SEARCH_SERVICE_ENABLED);
        if (Boolean.TRUE.equals(searchServiceEnabled)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS, IndexService.DEFAULT_TIMEOUT);
        }

        datasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS);
        keycloakService = KeyCloakService.createProxy(vertx, KeyCloakService.SERVICE_ADDRESS);

        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<String> listCatalogues(String acceptType, String valueType, Integer offset, Integer limit) {
        switch (valueType) {
            case "uriRefs" -> {
                return catalogueManager.listUris()
                        .map(list -> mapSublistToString(list, offset, limit, DCATAPUriRef::getUri))
                        .recover(HubRepo::failureReply);
            }
            case "identifiers", "originalIds" -> {
                return catalogueManager.listUris()
                        .map(list -> mapSublistToString(list, offset, limit, DCATAPUriRef::getId))
                        .recover(HubRepo::failureReply);
            }
            default -> {
                return catalogueManager.list(offset, limit)
                        .map(list -> Piveau.presentAs(list, acceptType, DCAT.Catalog))
                        .recover(HubRepo::failureReply);
            }
        }
    }

    @Override
    public Future<String> getCatalogue(String catalogueId, String acceptType) {
        return catalogueManager.ensureExists(catalogueId)
                .compose(v -> catalogueManager.getStripped(catalogueId))
                .map(model -> Piveau.presentAs(model, acceptType))
                .recover(HubRepo::failureReply);
    }

    @Override
    public Future<String> putCatalogue(String catalogueId, String content, String contentType) {
        CatalogueHelper catalogueHelper;
        try {
            catalogueHelper = new CatalogueHelper(catalogueId, contentType, content);
        } catch (RiotException e) {
            logger.error("Error parsing catalogue", e);
            return Future.failedFuture(new ServiceException(400, "Could not decode body with Content-Type " + contentType + ": " + e.getMessage()));
        }

        Model datasetsList = ModelFactory.createDefaultModel();
        return catalogueManager.exist(catalogueId)
                .compose(exists -> {
                    if (exists == Boolean.TRUE) {
                        // update
                        catalogueHelper.modified();
                        return catalogueManager.allDatasets(catalogueId)
                                .onSuccess(list ->
                                        list.forEach(dataset -> {
                                            datasetsList.add(catalogueHelper.resource(), DCAT.dataset, datasetsList.createResource(dataset.getUri()));
                                            datasetsList.add(catalogueHelper.resource(), DCAT.record, datasetsList.createResource(dataset.getRecordUriRef()));
                                        })
                                ).map("updated");
                    } else {
                        // create
                        return Future.succeededFuture("created");
                    }
                })
                .compose(result ->
                        catalogueManager.setGraph(catalogueHelper.uriRef(), catalogueHelper.getModel())
                                .onSuccess(uriRef -> keycloakService.createResource(catalogueHelper.getId()))
                                .map(result)
                )
                .compose(result -> {
                    if (datasetsList.isEmpty()) {
                        return Future.succeededFuture(result);
                    } else {
                        List<List<Statement>> chunks = Lists.partition(datasetsList.listStatements().toList(), 2500);
                        return tripleStore.postChunks(catalogueHelper.uriRef(), chunks).map(result);
                    }
                })
                .onSuccess(result -> {
                    if (!catalogueHelper.getModel()
                            .contains(catalogueHelper.resource(), EDP.visibility, EDP.hidden)) {
                        if (indexService != null) {
                            Indexing.indexingCatalogue(catalogueHelper.getModel().getResource(catalogueHelper.uriRef()))
                                    .onSuccess(index -> indexService.addCatalog(index)
                                            .onFailure(cause -> logger.error("Indexing new catalogue", cause))
                                    );
                        }
                    } else {
                        logger.info("Skip indexing for hidden catalogue");
                    }
                })
                .recover(HubRepo::failureReply);
    }

    @Override
    public Future<Void> deleteCatalogue(String id) {
        return catalogueManager.ensureExists(id)
                .compose(v -> catalogueManager.allDatasets(id))
                .compose(list -> {
                    List<Future<Void>> futures = list.stream().map(uriRef ->
                            datasetsService.deleteDataset(uriRef.getId())
                    ).toList();
                    return Future.join(futures).mapEmpty();
                })
                .compose(v -> catalogueManager.delete(id))
                .onSuccess(v -> {
                    if (indexService != null) {
                        indexService.deleteCatalog(id).onFailure(cause -> logger.error("Removing catalogue {} from index", id, cause));
                    }
                    keycloakService.deleteResource(id);
                })
                .recover(HubRepo::failureReply)
                .mapEmpty();
    }

    private String mapSublistToString(List<DCATAPUriRef> list, int offset, int limit, Function<? super DCATAPUriRef, String> mapper) {
        if (offset > list.size()) {
            return new JsonArray().encode();
        } else if ((offset + limit) > list.size()) {
            return new JsonArray(list.subList(offset, list.size()).stream().map(mapper).toList()).encodePrettily();
        } else {
            return new JsonArray(list.subList(offset, offset + limit).stream().map(mapper).toList()).encodePrettily();
        }
    }

}
