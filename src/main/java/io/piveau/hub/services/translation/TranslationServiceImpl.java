package io.piveau.hub.services.translation;

import io.piveau.dcatap.*;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.indexing.Indexing;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.Constants;
import io.piveau.json.ConfigHelper;
import io.piveau.log.PiveauLogger;
import io.piveau.utils.PiveauContext;
import io.piveau.vocabularies.vocabulary.EDP;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpResponseExpectation;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCTerms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static io.piveau.hub.services.translation.TranslationServiceUtils.*;

public class TranslationServiceImpl implements TranslationService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final DatasetManager datasetManager;
    private final CatalogueManager catalogueManager;

    private IndexService indexService;

    private final HttpRequest<Buffer> translationServiceRequest;

    private final PiveauContext moduleContext;

    TranslationServiceImpl(
            Vertx vertx,
            WebClient client,
            JsonObject config,
            TripleStore tripleStore,
            Handler<AsyncResult<TranslationService>> readyHandler) {

        ConfigHelper configHelper = ConfigHelper.forConfig(config);

        JsonObject translationConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_TRANSLATION_SERVICE);
        translationLanguages = translationConfig.getJsonArray("accepted_languages", new JsonArray())
                .stream()
                .map(Object::toString).toList();

        translationServiceRequest = client.postAbs(translationConfig.getString("translation_service_url", "https://example.com/translation-service/"))
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .timeout(10000);

        final String apiKey = config.getJsonObject(Constants.ENV_PIVEAU_HUB_API_KEYS, new JsonObject()).stream()
                .filter(entry -> entry.getValue() instanceof JsonArray)
                .filter(entry -> ((JsonArray) entry.getValue()).contains("*"))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(config.getString(Constants.ENV_PIVEAU_HUB_API_KEY));

        callbackParameters = new JsonObject()
                .put("url", translationConfig.getString("callback_url", "http://localhost:8080/translation"))
                .put("method", HttpMethod.POST.name())
                .put("headers", new JsonObject()
                        .put("X-API-Key", apiKey));

        datasetManager = tripleStore.getDatasetManager();
        catalogueManager = tripleStore.getCatalogueManager();

        JsonObject indexConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_SERVICE);
        if (indexConfig.getBoolean("enabled", false) == Boolean.TRUE) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS, IndexService.DEFAULT_TIMEOUT);
        }

        moduleContext = new PiveauContext("hub", "translation");

        readyHandler.handle(Future.succeededFuture(this));
    }

    /**
     * Extract title and description from Dataset and its Distributions and send them to the translation service.
     * <p>
     * If the Dataset is only an update of an old Dataset with no titles or descriptions changed, nothing will be send to the translation service.
     *
     * @param helper    the new Dataset with new and not yet translated fields
     * @param oldHelper the optional old Dataset with already translated fields, should be `null`, if there is no old Dataset
     * @return this {@link TranslationService} for a fluent usage
     */
    @Override
    public Future<DatasetHelper> initializeTranslationProcess(DatasetHelper helper, DatasetHelper oldHelper, Boolean force) {
        try {
            PiveauLogger logger = moduleContext.extend(helper.piveauId()).log();

            final JsonObject translationRequest = TranslationServiceUtils.buildTranslationRequest(helper, oldHelper, force);
            if (!translationRequest.isEmpty()) {
                initTranslationProcess(helper.recordResource(), helper.getCatalogueInfo().getString("sourceLang", "en"));
                translationServiceRequest.sendJsonObject(translationRequest)
                        .expecting(HttpResponseExpectation.SC_SUCCESS)
                        .onSuccess(response -> {
                            logger.info("Translation initialized.");
                            if (logger.isDebugEnabled()) {
                                logger.debug("Translation request: {}", translationRequest.encodePrettily());
                            }
                        })
                        .onFailure(cause -> logger.error("Translation initialization failed.", cause));
            }
            return Future.succeededFuture(helper);
        } catch (Exception e) {
            return Future.failedFuture(e);
        }
    }

    @Override
    public Future<JsonObject> receiveTranslation(JsonObject translation) {
            DCATAPUriRef uriRef = DCATAPUriSchema.parseUriRef(translation.getString("id"));

            PiveauContext resourceContext = moduleContext.extend(uriRef.getId());
            if (resourceContext.log().isDebugEnabled()) {
                resourceContext.log().debug("Incoming translation: {}", translation.encodePrettily());
            }

            AtomicBoolean hidden = new AtomicBoolean(false);

            return datasetManager.ensureExists(uriRef)
                    .compose(v -> datasetManager.getGraph(uriRef.getDatasetGraphName()))
                    .compose(model -> {

                        TranslationServiceUtils.applyTranslations(model, translation.getJsonObject("dict", new JsonObject()));

                        // Updating catalog record with translation information
                        Resource recordResource = model.getResource(uriRef.getRecordUriRef());
                        completeTranslationProcess(recordResource);

                        return Future.<Model>future(promise ->
                                datasetManager.setGraph(uriRef.getDatasetGraphName(), model, false)
                                        .onSuccess(id -> promise.complete(model))
                                        .onFailure(promise::fail)
                        );
                    })
                    .eventually(() ->
                        datasetManager.catalogue(uriRef.getDatasetUriRef())
                                .compose(resource -> {
                                    DCATAPUriRef catalogueUriRef = DCATAPUriSchema.parseUriRef(resource.getURI());
                                    return catalogueManager.getStrippedGraph(catalogueUriRef.getCatalogueUriRef());
                                })
                                .onSuccess(catalogueModel ->
                                    hidden.set(catalogueModel.contains(null, EDP.visibility, EDP.hidden))
                                )
                    )
                    .compose(model -> {
                        // We need to check the catalogue hidden value as well
                        if (indexService != null && !hidden.get()) {
                            Resource recordResource = model.getResource(uriRef.getRecordUriRef());
                            // We need catalogue id and sourceLang (default language)
                            JsonObject data = translation.getJsonObject("data", new JsonObject());
                            JsonObject index = Indexing.indexingDataset(
                                    model.getResource(uriRef.getDatasetUriRef()),
                                    recordResource,
                                    data.getString("catalogueId"),
                                    data.getString("defaultLanguage"));

                            indexService.addDatasetPut(index)
                                    .onSuccess(result -> log.debug("Dataset successfully indexed"))
                                    .onFailure(cause -> log.error("Index dataset", cause));
                        }
                        resourceContext.log().info("Translation stored");
                        return Future.succeededFuture(new JsonObject().put("status", "success"));
                    })
                    .onFailure(cause -> resourceContext.log().error("Translation", cause));
    }

    private void initTranslationProcess(Resource recordResource, String sourceLang) {
        recordResource.removeAll(EDP.originalLanguage);
        recordResource.addProperty(EDP.originalLanguage, sourceLang);
        recordResource.removeAll(EDP.transReceived);
        recordResource.removeAll(EDP.transIssued);
        recordResource.addProperty(EDP.transIssued, ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
        recordResource.removeAll(EDP.transStatus);
        recordResource.addProperty(EDP.transStatus, EDP.TransInProcess);
    }

    private void completeTranslationProcess(Resource recordResource) {
        recordResource.removeAll(EDP.transReceived);
        recordResource.addProperty(EDP.transReceived, ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
        recordResource.removeAll(EDP.transStatus);
        recordResource.addProperty(EDP.transStatus, EDP.TransCompleted);

        recordResource.removeAll(DCTerms.modified);
        recordResource.addProperty(DCTerms.modified, ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);
    }

}
