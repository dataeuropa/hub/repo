package io.piveau.hub.services.translation;

import io.piveau.dcatap.TripleStore;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

@ProxyGen
public interface TranslationService {
    String SERVICE_ADDRESS = "io.piveau.hub.translationservice.queue";

    static Future<TranslationService> create(Vertx vertx, WebClient client, JsonObject config, TripleStore tripleStore) {
        return Future.future(promise -> new TranslationServiceImpl(vertx, client, config, tripleStore, promise));
    }

    static TranslationService createProxy(Vertx vertx, String address) {
        return new TranslationServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    Future<DatasetHelper> initializeTranslationProcess(DatasetHelper helper, DatasetHelper oldHelper, Boolean force);

    Future<JsonObject> receiveTranslation(JsonObject translation);

}
