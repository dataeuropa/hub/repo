package io.piveau.hub.services.index;

import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpResponseExpectation;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.serviceproxy.ServiceException;

public class IndexServiceImpl implements IndexService {

    private static final String AUTH_HEADER = "X-API-Key";
    private static final String JSON_CONTENT_TYPE = "application/json";
    private static final String DATASETS_CONTEXT = "/datasets";

    private static final String EMPTY_INDEX_OBJECT = "Nothing to index";

    private final WebClient client;

    private final Integer port;
    private final String url;
    private final String apiKey;

    IndexServiceImpl(WebClient client, JsonObject config, Handler<AsyncResult<IndexService>> readyHandler) {
        this.client = client;

        this.port = config.getInteger("port", 8080);
        this.url = config.getString("url", "piveau-hub-search");
        this.apiKey = config.getString("api_key", "");

        readyHandler.handle(Future.succeededFuture(this));
    }

    /**
     * Sends a dataset to the search service
     *
     * @param dataset The dataset
     */
    @Override
    public Future<JsonObject> addDataset(JsonObject dataset) {
        return entityExists(dataset.getString("id"))
                .map(exists -> {
                    if (exists == Boolean.TRUE) {
                        return client.put(this.port, this.url, DATASETS_CONTEXT + "/" + dataset.getString("id").replace("*", "%2A"));
                    } else {
                        return client.post(this.port, this.url, DATASETS_CONTEXT);
                    }
                })
                .compose(request ->
                    request.putHeader(AUTH_HEADER, this.apiKey)
                            .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                            .timeout(300000) // 5 min
                            .sendJsonObject(dataset))
                .map(new JsonObject());
    }

    @Override
    public Future<JsonObject> addDatasetPut(JsonObject dataset) {
        if (dataset.isEmpty()) {
            return Future.failedFuture(EMPTY_INDEX_OBJECT);
        }

        return client.put(this.port, this.url, DATASETS_CONTEXT + "/" + dataset.getString("id").replace("*", "%2A"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .addQueryParam("synchronous", "true")
                .sendJsonObject(dataset)
                .map(response -> {
                    if (response.statusCode() == 200 || response.statusCode() == 201) {
                        return dataset;
                    } else if (response.statusCode() == 400) {
                        throw new ServiceException(400, response.body().toString());
                    } else {
                        throw new ServiceException(response.statusCode(), response.statusMessage());
                    }
                });
    }

    @Override
    public Future<JsonObject> modifyDataset(String datasetId, JsonObject dataset) {
        if (dataset.isEmpty()) {
            return Future.failedFuture(EMPTY_INDEX_OBJECT);
        }

        if (datasetId == null || datasetId.isBlank()) {
            return Future.failedFuture("Dataset id is empty");
        }

        return client.patch(this.port, this.url, DATASETS_CONTEXT + "/" + datasetId.replace("*", "%2A"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(300000)
                .sendJsonObject(dataset)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(new JsonObject());
    }

    @Override
    public Future<JsonArray> putVocabulary(String vocabularyId, JsonObject vocabulary) {
        return client.put(this.port, this.url, "/vocabularies/" + vocabularyId)
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(240000) // 4 min
                .sendJsonObject(vocabulary)
                .map(response -> {
                    if (response.statusCode() == 200 || response.statusCode() == 201) {
                        return response.body().toJsonArray();
                    } else if (response.statusCode() == 400) {
                        throw new ServiceException(response.statusCode(), response.statusMessage(), response.body().toJsonObject());
                    } else {
                        throw new ServiceException(response.statusCode(), response.statusMessage());
                    }
                });
    }

    @Override
    public Future<JsonObject> deleteVocabulary(String vocabularyId) {
        return client.delete(this.port, this.url, "/vocabularies/" + vocabularyId)
                .putHeader(AUTH_HEADER, this.apiKey)
                .timeout(60000) // 1 min
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(new JsonObject());
    }

    @Override
    public Future<JsonObject> deleteDataset(String datasetId) {
        return client.delete(this.port, this.url, DATASETS_CONTEXT + "/" + datasetId.replace("*", "%2A"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .timeout(60000) // 1 min
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(new JsonObject());
    }

    @Override
    public Future<JsonArray> listDatasets(String catalogue, String alias) {
        return client.get(this.port, this.url, DATASETS_CONTEXT)
                .setQueryParam("catalogue", catalogue)
                .setQueryParam("alias", alias)
                .timeout(300000) // 5 min
                .send()
                .expecting(HttpResponseExpectation.SC_OK)
                .map(response -> response.body().toJsonArray());
    }

    @Override
    public Future<JsonObject> deleteCatalog(String catalogId) {
        return client.delete(this.port, this.url, "/catalogues/" + catalogId)
                .putHeader(AUTH_HEADER, this.apiKey)
                .timeout(300000) // 5 min
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(new JsonObject());
    }

    /**
     * Sends a catalog to the search service
     *
     * @param catalog The catalogue
     */
    @Override
    public Future<JsonObject> addCatalog(JsonObject catalog) {
        return client.put(this.port, this.url, "/catalogues/" + catalog.getString("id"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .sendJsonObject(catalog)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(new JsonObject());
    }

    @Override
    public Future<JsonObject> getDataset(String id) {
        return client.get(this.port, this.url, DATASETS_CONTEXT + "/" + id.replace("*", "%2A"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.ACCEPT.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonObject());
    }

    @Override
    public Future<JsonArray> putDatasetBulk(JsonArray datasets) {
        if (datasets.isEmpty()) {
            return Future.failedFuture(EMPTY_INDEX_OBJECT);
        }

        JsonObject datasetsBulk = new JsonObject().put("datasets", datasets);

        return client.put(this.port, this.url, "/bulk/datasets")
                .addQueryParam("synchronous", "true")
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .sendJsonObject(datasetsBulk)
                .map(response -> {
                    if (response.statusCode() == 200 || response.statusCode() == 201) {
                        return response.bodyAsJsonArray();
                    } else {
                        JsonObject debug = new JsonObject();
                        if (response.body() != null && response.body().toJsonObject() != null) {
                            debug = response.body().toJsonObject();
                        }
                        throw new ServiceException(response.statusCode(), response.statusMessage(), debug);
                    }
                });
    }

    @Override
    public Future<Void> putResource(String id, String type, JsonObject payload) {
        return client.put(this.port, this.url, "/resources/" + type + "/" + id)
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .sendJsonObject(payload)
                .expecting(HttpResponseExpectation.SC_NO_CONTENT.or(HttpResponseExpectation.SC_CREATED))
                .mapEmpty();
    }

    @Override
    public Future<Void> deleteResource(String id, String type) {
        return client.delete(this.port, this.url, "/resources/" + type + "/" + id)
                .putHeader(AUTH_HEADER, this.apiKey)
                .timeout(300000) // 5 min
                .send()
                .expecting(HttpResponseExpectation.SC_NO_CONTENT)
                .mapEmpty();
    }

    private Future<Boolean> entityExists(String id) {
        return client.head(this.port, this.url, DATASETS_CONTEXT + "/" + id.replace("*", "%2A"))
                .putHeader(AUTH_HEADER, this.apiKey)
                .putHeader(HttpHeaders.ACCEPT.toString(), JSON_CONTENT_TYPE)
                .timeout(300000) // 5 min
                .send()
                .expecting(HttpResponseExpectation.SC_OK.or(HttpResponseExpectation.SC_NOT_FOUND))
                .map(response -> response.statusCode() == 200);
    }

}
