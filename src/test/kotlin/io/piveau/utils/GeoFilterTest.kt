package io.piveau.utils

import io.piveau.fixGeoDatatypes
import io.piveau.rdf.presentAsTurtle
import io.piveau.rdf.toModel
import io.piveau.vocabularies.vocabulary.LOCN
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.graph.NodeFactory
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.riot.Lang
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertTrue

@DisplayName("Testing Catalogues API")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GeoFilterTest {

    @Test
    fun `Testing virtrdf geo datatype filter`(vertx: Vertx, testContext: VertxTestContext) {
        vertx.fileSystem().readFile("update_example.ttl")
            .map { it.bytes.toModel(Lang.TURTLE) }
            .onSuccess {
                fixGeoDatatypes(it)
                testContext.verify {
                    assertTrue {
                        it.contains(
                            null,
                            LOCN.geometry,
                            ResourceFactory.createTypedLiteral(
                                "BOX(12.73 56.41,14.52 57.61)",
                                NodeFactory.getType("http://www.opengis.net/ont/geosparql#wktLiteral")
                            )
                        )
                    }
                    testContext.completeNow()
                }
            }
            .onFailure { testContext.failNow(it) }
    }

}